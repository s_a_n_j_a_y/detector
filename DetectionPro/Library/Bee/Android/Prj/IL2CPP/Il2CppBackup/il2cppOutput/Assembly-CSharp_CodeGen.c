﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Detector::Start()
extern void Detector_Start_mD69279C5B5CE102BCB490E346CB0E4679C148C43 (void);
// 0x00000002 System.Collections.IEnumerator Detector::Detect(UnityEngine.Color32[],System.Action`1<System.Collections.Generic.IList`1<BoundingBox>>)
extern void Detector_Detect_mF7E06C6A16E6005CBA62AC5BBD89CABD3032E12E (void);
// 0x00000003 Unity.Barracuda.Tensor Detector::TransformInput(UnityEngine.Color32[],System.Int32,System.Int32)
extern void Detector_TransformInput_m70DB7B82E6F85F933AEBB34D98CCD62F67C12116 (void);
// 0x00000004 System.Collections.Generic.IList`1<BoundingBox> Detector::ParseOutputs(Unity.Barracuda.Tensor,System.Single)
extern void Detector_ParseOutputs_m43D3C60ECD5961E7398A77E019855B701EA97D05 (void);
// 0x00000005 System.Single Detector::Sigmoid(System.Single)
extern void Detector_Sigmoid_mCF05C6BD26BEC3884F205912F1BD56865E3B28D0 (void);
// 0x00000006 System.Single[] Detector::Softmax(System.Single[])
extern void Detector_Softmax_mC92F5060627D870E16B67ABE6FE364F25D2C16CA (void);
// 0x00000007 System.Single Detector::GetConfidence(Unity.Barracuda.Tensor,System.Int32,System.Int32,System.Int32)
extern void Detector_GetConfidence_mD04080750424C52D2710A6385C9A80FAC64C54AE (void);
// 0x00000008 System.Single[] Detector::ExtractClasses(Unity.Barracuda.Tensor,System.Int32,System.Int32,System.Int32)
extern void Detector_ExtractClasses_mBE2BC473855937D06B745973F3745B8A51299806 (void);
// 0x00000009 System.ValueTuple`2<System.Int32,System.Single> Detector::GetTopResult(System.Single[])
extern void Detector_GetTopResult_m77F06CCB4F2570BD7D8F9A8E5877108CEA0B610B (void);
// 0x0000000A System.Void Detector::.ctor()
extern void Detector__ctor_mAD82AD1BAB8A450465B81A86CAB292C8A3EBCB3E (void);
// 0x0000000B System.Void Detector/<Detect>d__18::.ctor(System.Int32)
extern void U3CDetectU3Ed__18__ctor_mDC24DE8F8D42EAEBF097B384F1D425CBFEDC01DA (void);
// 0x0000000C System.Void Detector/<Detect>d__18::System.IDisposable.Dispose()
extern void U3CDetectU3Ed__18_System_IDisposable_Dispose_m612F41816C95EE746B09DFF12C97156E88C27218 (void);
// 0x0000000D System.Boolean Detector/<Detect>d__18::MoveNext()
extern void U3CDetectU3Ed__18_MoveNext_mD6488F35519BAD5A786DEE3361502045F1FEACFD (void);
// 0x0000000E System.Void Detector/<Detect>d__18::<>m__Finally1()
extern void U3CDetectU3Ed__18_U3CU3Em__Finally1_m21985138C6A5A3A76754E0480D83BA0AA29FAA94 (void);
// 0x0000000F System.Object Detector/<Detect>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDetectU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52E50F5C28F3897B382E49C3A5DA81B2E6CFC2B0 (void);
// 0x00000010 System.Void Detector/<Detect>d__18::System.Collections.IEnumerator.Reset()
extern void U3CDetectU3Ed__18_System_Collections_IEnumerator_Reset_m20841F6BEFEF1B50F595EE9CAC1373A72B6EAC0B (void);
// 0x00000011 System.Object Detector/<Detect>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CDetectU3Ed__18_System_Collections_IEnumerator_get_Current_m0888C367CFA15FA1C56768471593FEA6A7DF361D (void);
// 0x00000012 System.Void Detector/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m31724DA5864B4C47CF318E8E1956EAF7D971E383 (void);
// 0x00000013 System.Double Detector/<>c__DisplayClass22_0::<Softmax>b__0(System.Single)
extern void U3CU3Ec__DisplayClass22_0_U3CSoftmaxU3Eb__0_m77CBFD936F493D01E896B0E518BC436EE3B3845E (void);
// 0x00000014 System.Single Detector/<>c__DisplayClass22_0::<Softmax>b__1(System.Double)
extern void U3CU3Ec__DisplayClass22_0_U3CSoftmaxU3Eb__1_mE9F6EA14222CF19AF3BDA5217A977C5BAEAFE5DF (void);
// 0x00000015 System.Void Detector/<>c::.cctor()
extern void U3CU3Ec__cctor_mEF8FE2A305DEFF27220E5E055CF2690D7896C4F3 (void);
// 0x00000016 System.Void Detector/<>c::.ctor()
extern void U3CU3Ec__ctor_m67170BF871ACE0B683BA53B3E1B6F6104C866FD8 (void);
// 0x00000017 System.ValueTuple`2<System.Int32,System.Single> Detector/<>c::<GetTopResult>b__25_0(System.Single,System.Int32)
extern void U3CU3Ec_U3CGetTopResultU3Eb__25_0_m6BF281AB15D6FA7FEAF74F680602ED2A2EA4781D (void);
// 0x00000018 System.Single Detector/<>c::<GetTopResult>b__25_1(System.ValueTuple`2<System.Int32,System.Single>)
extern void U3CU3Ec_U3CGetTopResultU3Eb__25_1_m8954BE50605939235DCDA5D794EE0A7245021E74 (void);
// 0x00000019 System.Single DimensionsBase::get_X()
extern void DimensionsBase_get_X_m2AA39E00503C51C587F473D1ADBEC9CCB5D54F41 (void);
// 0x0000001A System.Void DimensionsBase::set_X(System.Single)
extern void DimensionsBase_set_X_mD9B14D9F026F7773AC1821A15DEB1F1040C80D7A (void);
// 0x0000001B System.Single DimensionsBase::get_Y()
extern void DimensionsBase_get_Y_m170190F8D1531F544A40AB55817C1EC7EFD8A864 (void);
// 0x0000001C System.Void DimensionsBase::set_Y(System.Single)
extern void DimensionsBase_set_Y_m1A01B024CAB8953A5D9317F3AB2713D0F3102E8F (void);
// 0x0000001D System.Single DimensionsBase::get_Height()
extern void DimensionsBase_get_Height_m6632C66BBCA8F5D5ABC7E95739DD3713DBA7E9BE (void);
// 0x0000001E System.Void DimensionsBase::set_Height(System.Single)
extern void DimensionsBase_set_Height_m31B2BDC1EEE04DDD2AF3AFD0AEFE49F3646CB28B (void);
// 0x0000001F System.Single DimensionsBase::get_Width()
extern void DimensionsBase_get_Width_m6B20834F813A905E59F902C802FAB97D21286ECF (void);
// 0x00000020 System.Void DimensionsBase::set_Width(System.Single)
extern void DimensionsBase_set_Width_m9DB1DEBCC8DBA0E0EC7D7AE2EE499AA6D7304EE4 (void);
// 0x00000021 System.Void DimensionsBase::.ctor()
extern void DimensionsBase__ctor_m9AFA3033ED5147C13EC158B85DC75513599B0585 (void);
// 0x00000022 System.Void BoundingBoxDimensions::.ctor()
extern void BoundingBoxDimensions__ctor_mEF132B75F7213E091DCF01A4B298C92F86E90324 (void);
// 0x00000023 System.Void CellDimensions::.ctor()
extern void CellDimensions__ctor_m244B9B96048567D9FCAF6AFFCA64FEA4391EDC55 (void);
// 0x00000024 BoundingBoxDimensions BoundingBox::get_Dimensions()
extern void BoundingBox_get_Dimensions_m4F3FC84E34A2F040280CA9119B6AE2C9222F55AC (void);
// 0x00000025 System.Void BoundingBox::set_Dimensions(BoundingBoxDimensions)
extern void BoundingBox_set_Dimensions_mB6A4FF8905F4A0B36C7BFECB44B47CED60B24E0E (void);
// 0x00000026 System.String BoundingBox::get_Label()
extern void BoundingBox_get_Label_m6C86B30C4199D01AA9838287FEA1580EA9C3DA2D (void);
// 0x00000027 System.Void BoundingBox::set_Label(System.String)
extern void BoundingBox_set_Label_m30A8EF56970E2FA83C37093F48CDAE3565B0C20D (void);
// 0x00000028 System.Single BoundingBox::get_Confidence()
extern void BoundingBox_get_Confidence_m7930AB5EF72D84E6FCFB1432F5F62F169BB1C3A2 (void);
// 0x00000029 System.Void BoundingBox::set_Confidence(System.Single)
extern void BoundingBox_set_Confidence_m4321A9367A73A6ABFF035C71565B9F42879B8F9F (void);
// 0x0000002A UnityEngine.Rect BoundingBox::get_Rect()
extern void BoundingBox_get_Rect_m82F8A4EE4490F2EF0787A883BFA133B5D7F65C9A (void);
// 0x0000002B System.String BoundingBox::ToString()
extern void BoundingBox_ToString_m6DA8C8A5218D472F3B4BB8B459BDC065D937358B (void);
// 0x0000002C System.Void BoundingBox::.ctor()
extern void BoundingBox__ctor_m8E37599BC48C6E824104EC1C4A83FDA8ABA88C52 (void);
// 0x0000002D System.Void PhoneCamera::Start()
extern void PhoneCamera_Start_m750FC2C73FCF97B24D20AAB83749E2CC495E60B1 (void);
// 0x0000002E System.Void PhoneCamera::Update()
extern void PhoneCamera_Update_m9F6D7E0379A70DAC4A8C92DFD173B219F2707BEE (void);
// 0x0000002F System.Void PhoneCamera::CalculateShift(System.Int32)
extern void PhoneCamera_CalculateShift_mB728F0C3229A2001D59AE21D5EF675726F23F608 (void);
// 0x00000030 System.Void PhoneCamera::TFDetect()
extern void PhoneCamera_TFDetect_mFF76488480372773AF1F894433E4DAC1FFCB8E80 (void);
// 0x00000031 System.Collections.IEnumerator PhoneCamera::ProcessImage(System.Int32,System.Action`1<UnityEngine.Color32[]>)
extern void PhoneCamera_ProcessImage_m68C34D4C3E9C403AE677F921D15E7B5D06EF650E (void);
// 0x00000032 UnityEngine.Texture2D PhoneCamera::Scale(UnityEngine.Texture2D,System.Int32)
extern void PhoneCamera_Scale_m9A8BA55FFE200580A45E79CAD587A84A5CA54908 (void);
// 0x00000033 UnityEngine.Color32[] PhoneCamera::Rotate(UnityEngine.Color32[],System.Int32,System.Int32)
extern void PhoneCamera_Rotate_m52B13D25E4B4F2222D4B09FDF6E68D4F38CDB84A (void);
// 0x00000034 System.Void PhoneCamera::.ctor()
extern void PhoneCamera__ctor_mBE881DC2D28EFE418D69A390D74C70C532F380C3 (void);
// 0x00000035 System.Void PhoneCamera::<TFDetect>b__13_0(UnityEngine.Color32[])
extern void PhoneCamera_U3CTFDetectU3Eb__13_0_m8C397BA0EB24746D6967E6F04C682C86B540BD48 (void);
// 0x00000036 System.Void PhoneCamera::<TFDetect>b__13_1(System.Collections.Generic.IList`1<BoundingBox>)
extern void PhoneCamera_U3CTFDetectU3Eb__13_1_mAD506FF440893CFFD070C52A1CBBDFF359DB3F51 (void);
// 0x00000037 System.Void PhoneCamera/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_mBCB0D3117D67C026EBB00245AFD9AB9A7E8962C8 (void);
// 0x00000038 System.Void PhoneCamera/<>c__DisplayClass14_0::<ProcessImage>b__0(UnityEngine.Texture2D)
extern void U3CU3Ec__DisplayClass14_0_U3CProcessImageU3Eb__0_m3E7C0BDCC16F0E5A81D725A7A4547CE8DE688FB5 (void);
// 0x00000039 System.Void PhoneCamera/<ProcessImage>d__14::.ctor(System.Int32)
extern void U3CProcessImageU3Ed__14__ctor_m64F65AE69E02E4D6DA72E4A43FD9E2E4D712C584 (void);
// 0x0000003A System.Void PhoneCamera/<ProcessImage>d__14::System.IDisposable.Dispose()
extern void U3CProcessImageU3Ed__14_System_IDisposable_Dispose_mD3B8D74F60C018E854202415357CA5496FBD720D (void);
// 0x0000003B System.Boolean PhoneCamera/<ProcessImage>d__14::MoveNext()
extern void U3CProcessImageU3Ed__14_MoveNext_m7D790D0E94C4F240CFB990248065EE3640AC4D64 (void);
// 0x0000003C System.Object PhoneCamera/<ProcessImage>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessImageU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m322184F6BAFE67C5FD9B78B68BBD8521BA4C8E6C (void);
// 0x0000003D System.Void PhoneCamera/<ProcessImage>d__14::System.Collections.IEnumerator.Reset()
extern void U3CProcessImageU3Ed__14_System_Collections_IEnumerator_Reset_mF3E80C2683A1452A45277CD2FA554C0AE7CC6B89 (void);
// 0x0000003E System.Object PhoneCamera/<ProcessImage>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CProcessImageU3Ed__14_System_Collections_IEnumerator_get_Current_mCD5396DFDE91EA5F055C2D1DE9399BD8245C19FE (void);
// 0x0000003F System.Collections.IEnumerator TFClassify.TextureTools::CropSquare(UnityEngine.WebCamTexture,TFClassify.TextureTools/RectOptions,System.Action`1<UnityEngine.Texture2D>)
extern void TextureTools_CropSquare_m8CFFF250CD0D934710F6D15DE737D91AAE29B96E (void);
// 0x00000040 UnityEngine.Texture2D TFClassify.TextureTools::scaled(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.FilterMode)
extern void TextureTools_scaled_m42BE122AF1DC9EBC821B957472F4E956F8395885 (void);
// 0x00000041 System.Void TFClassify.TextureTools::_gpu_scale(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.FilterMode)
extern void TextureTools__gpu_scale_m41EB6FEFF1926369E00E6254F6F07C2B8BEDE8DF (void);
// 0x00000042 UnityEngine.Color32[] TFClassify.TextureTools::RotateImageMatrix(UnityEngine.Color32[],System.Int32,System.Int32,System.Int32)
extern void TextureTools_RotateImageMatrix_m6042CB8FC50C973D9A2D1D1DBE53E21435BF073B (void);
// 0x00000043 UnityEngine.Color32[] TFClassify.TextureTools::rotateSquare(UnityEngine.Color32[],System.Int32,System.Int32,System.Double)
extern void TextureTools_rotateSquare_m39D669E9FC88B16402F908880E5796BA7749E2DF (void);
// 0x00000044 System.Void TFClassify.TextureTools::.ctor()
extern void TextureTools__ctor_mFCE1EC2FA0A438A35626FE5FA3CA479A79F5A109 (void);
// 0x00000045 System.Void TFClassify.TextureTools/<CropSquare>d__2::.ctor(System.Int32)
extern void U3CCropSquareU3Ed__2__ctor_mFD8FDCD0E558E01DE3C96314D12284483696A1B3 (void);
// 0x00000046 System.Void TFClassify.TextureTools/<CropSquare>d__2::System.IDisposable.Dispose()
extern void U3CCropSquareU3Ed__2_System_IDisposable_Dispose_m9071D08371C1CB5BC70AF7711C91EB59EDB02BDA (void);
// 0x00000047 System.Boolean TFClassify.TextureTools/<CropSquare>d__2::MoveNext()
extern void U3CCropSquareU3Ed__2_MoveNext_mC58C96CCC338EA44F5D3BA39E81E547C28E15094 (void);
// 0x00000048 System.Object TFClassify.TextureTools/<CropSquare>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCropSquareU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18CC5480C17CDC221D5A7941F4E9A99858D1510B (void);
// 0x00000049 System.Void TFClassify.TextureTools/<CropSquare>d__2::System.Collections.IEnumerator.Reset()
extern void U3CCropSquareU3Ed__2_System_Collections_IEnumerator_Reset_m08EFD2ED4457D7E2CCF13F35B9204722A3493FDB (void);
// 0x0000004A System.Object TFClassify.TextureTools/<CropSquare>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CCropSquareU3Ed__2_System_Collections_IEnumerator_get_Current_mF8DD69C9A46B4CDDDE50791F9CE1372EACB1BB23 (void);
static Il2CppMethodPointer s_methodPointers[74] = 
{
	Detector_Start_mD69279C5B5CE102BCB490E346CB0E4679C148C43,
	Detector_Detect_mF7E06C6A16E6005CBA62AC5BBD89CABD3032E12E,
	Detector_TransformInput_m70DB7B82E6F85F933AEBB34D98CCD62F67C12116,
	Detector_ParseOutputs_m43D3C60ECD5961E7398A77E019855B701EA97D05,
	Detector_Sigmoid_mCF05C6BD26BEC3884F205912F1BD56865E3B28D0,
	Detector_Softmax_mC92F5060627D870E16B67ABE6FE364F25D2C16CA,
	Detector_GetConfidence_mD04080750424C52D2710A6385C9A80FAC64C54AE,
	Detector_ExtractClasses_mBE2BC473855937D06B745973F3745B8A51299806,
	Detector_GetTopResult_m77F06CCB4F2570BD7D8F9A8E5877108CEA0B610B,
	Detector__ctor_mAD82AD1BAB8A450465B81A86CAB292C8A3EBCB3E,
	U3CDetectU3Ed__18__ctor_mDC24DE8F8D42EAEBF097B384F1D425CBFEDC01DA,
	U3CDetectU3Ed__18_System_IDisposable_Dispose_m612F41816C95EE746B09DFF12C97156E88C27218,
	U3CDetectU3Ed__18_MoveNext_mD6488F35519BAD5A786DEE3361502045F1FEACFD,
	U3CDetectU3Ed__18_U3CU3Em__Finally1_m21985138C6A5A3A76754E0480D83BA0AA29FAA94,
	U3CDetectU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52E50F5C28F3897B382E49C3A5DA81B2E6CFC2B0,
	U3CDetectU3Ed__18_System_Collections_IEnumerator_Reset_m20841F6BEFEF1B50F595EE9CAC1373A72B6EAC0B,
	U3CDetectU3Ed__18_System_Collections_IEnumerator_get_Current_m0888C367CFA15FA1C56768471593FEA6A7DF361D,
	U3CU3Ec__DisplayClass22_0__ctor_m31724DA5864B4C47CF318E8E1956EAF7D971E383,
	U3CU3Ec__DisplayClass22_0_U3CSoftmaxU3Eb__0_m77CBFD936F493D01E896B0E518BC436EE3B3845E,
	U3CU3Ec__DisplayClass22_0_U3CSoftmaxU3Eb__1_mE9F6EA14222CF19AF3BDA5217A977C5BAEAFE5DF,
	U3CU3Ec__cctor_mEF8FE2A305DEFF27220E5E055CF2690D7896C4F3,
	U3CU3Ec__ctor_m67170BF871ACE0B683BA53B3E1B6F6104C866FD8,
	U3CU3Ec_U3CGetTopResultU3Eb__25_0_m6BF281AB15D6FA7FEAF74F680602ED2A2EA4781D,
	U3CU3Ec_U3CGetTopResultU3Eb__25_1_m8954BE50605939235DCDA5D794EE0A7245021E74,
	DimensionsBase_get_X_m2AA39E00503C51C587F473D1ADBEC9CCB5D54F41,
	DimensionsBase_set_X_mD9B14D9F026F7773AC1821A15DEB1F1040C80D7A,
	DimensionsBase_get_Y_m170190F8D1531F544A40AB55817C1EC7EFD8A864,
	DimensionsBase_set_Y_m1A01B024CAB8953A5D9317F3AB2713D0F3102E8F,
	DimensionsBase_get_Height_m6632C66BBCA8F5D5ABC7E95739DD3713DBA7E9BE,
	DimensionsBase_set_Height_m31B2BDC1EEE04DDD2AF3AFD0AEFE49F3646CB28B,
	DimensionsBase_get_Width_m6B20834F813A905E59F902C802FAB97D21286ECF,
	DimensionsBase_set_Width_m9DB1DEBCC8DBA0E0EC7D7AE2EE499AA6D7304EE4,
	DimensionsBase__ctor_m9AFA3033ED5147C13EC158B85DC75513599B0585,
	BoundingBoxDimensions__ctor_mEF132B75F7213E091DCF01A4B298C92F86E90324,
	CellDimensions__ctor_m244B9B96048567D9FCAF6AFFCA64FEA4391EDC55,
	BoundingBox_get_Dimensions_m4F3FC84E34A2F040280CA9119B6AE2C9222F55AC,
	BoundingBox_set_Dimensions_mB6A4FF8905F4A0B36C7BFECB44B47CED60B24E0E,
	BoundingBox_get_Label_m6C86B30C4199D01AA9838287FEA1580EA9C3DA2D,
	BoundingBox_set_Label_m30A8EF56970E2FA83C37093F48CDAE3565B0C20D,
	BoundingBox_get_Confidence_m7930AB5EF72D84E6FCFB1432F5F62F169BB1C3A2,
	BoundingBox_set_Confidence_m4321A9367A73A6ABFF035C71565B9F42879B8F9F,
	BoundingBox_get_Rect_m82F8A4EE4490F2EF0787A883BFA133B5D7F65C9A,
	BoundingBox_ToString_m6DA8C8A5218D472F3B4BB8B459BDC065D937358B,
	BoundingBox__ctor_m8E37599BC48C6E824104EC1C4A83FDA8ABA88C52,
	PhoneCamera_Start_m750FC2C73FCF97B24D20AAB83749E2CC495E60B1,
	PhoneCamera_Update_m9F6D7E0379A70DAC4A8C92DFD173B219F2707BEE,
	PhoneCamera_CalculateShift_mB728F0C3229A2001D59AE21D5EF675726F23F608,
	PhoneCamera_TFDetect_mFF76488480372773AF1F894433E4DAC1FFCB8E80,
	PhoneCamera_ProcessImage_m68C34D4C3E9C403AE677F921D15E7B5D06EF650E,
	PhoneCamera_Scale_m9A8BA55FFE200580A45E79CAD587A84A5CA54908,
	PhoneCamera_Rotate_m52B13D25E4B4F2222D4B09FDF6E68D4F38CDB84A,
	PhoneCamera__ctor_mBE881DC2D28EFE418D69A390D74C70C532F380C3,
	PhoneCamera_U3CTFDetectU3Eb__13_0_m8C397BA0EB24746D6967E6F04C682C86B540BD48,
	PhoneCamera_U3CTFDetectU3Eb__13_1_mAD506FF440893CFFD070C52A1CBBDFF359DB3F51,
	U3CU3Ec__DisplayClass14_0__ctor_mBCB0D3117D67C026EBB00245AFD9AB9A7E8962C8,
	U3CU3Ec__DisplayClass14_0_U3CProcessImageU3Eb__0_m3E7C0BDCC16F0E5A81D725A7A4547CE8DE688FB5,
	U3CProcessImageU3Ed__14__ctor_m64F65AE69E02E4D6DA72E4A43FD9E2E4D712C584,
	U3CProcessImageU3Ed__14_System_IDisposable_Dispose_mD3B8D74F60C018E854202415357CA5496FBD720D,
	U3CProcessImageU3Ed__14_MoveNext_m7D790D0E94C4F240CFB990248065EE3640AC4D64,
	U3CProcessImageU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m322184F6BAFE67C5FD9B78B68BBD8521BA4C8E6C,
	U3CProcessImageU3Ed__14_System_Collections_IEnumerator_Reset_mF3E80C2683A1452A45277CD2FA554C0AE7CC6B89,
	U3CProcessImageU3Ed__14_System_Collections_IEnumerator_get_Current_mCD5396DFDE91EA5F055C2D1DE9399BD8245C19FE,
	TextureTools_CropSquare_m8CFFF250CD0D934710F6D15DE737D91AAE29B96E,
	TextureTools_scaled_m42BE122AF1DC9EBC821B957472F4E956F8395885,
	TextureTools__gpu_scale_m41EB6FEFF1926369E00E6254F6F07C2B8BEDE8DF,
	TextureTools_RotateImageMatrix_m6042CB8FC50C973D9A2D1D1DBE53E21435BF073B,
	TextureTools_rotateSquare_m39D669E9FC88B16402F908880E5796BA7749E2DF,
	TextureTools__ctor_mFCE1EC2FA0A438A35626FE5FA3CA479A79F5A109,
	U3CCropSquareU3Ed__2__ctor_mFD8FDCD0E558E01DE3C96314D12284483696A1B3,
	U3CCropSquareU3Ed__2_System_IDisposable_Dispose_m9071D08371C1CB5BC70AF7711C91EB59EDB02BDA,
	U3CCropSquareU3Ed__2_MoveNext_mC58C96CCC338EA44F5D3BA39E81E547C28E15094,
	U3CCropSquareU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18CC5480C17CDC221D5A7941F4E9A99858D1510B,
	U3CCropSquareU3Ed__2_System_Collections_IEnumerator_Reset_m08EFD2ED4457D7E2CCF13F35B9204722A3493FDB,
	U3CCropSquareU3Ed__2_System_Collections_IEnumerator_get_Current_mF8DD69C9A46B4CDDDE50791F9CE1372EACB1BB23,
};
static const int32_t s_InvokerIndices[74] = 
{
	4986,
	1751,
	6749,
	1752,
	3640,
	3580,
	746,
	719,
	2557,
	4986,
	3996,
	4986,
	4814,
	4986,
	4885,
	4986,
	4885,
	4986,
	3151,
	3635,
	9681,
	4986,
	1308,
	3633,
	4924,
	4051,
	4924,
	4051,
	4924,
	4051,
	4924,
	4051,
	4986,
	4986,
	4986,
	4885,
	4020,
	4885,
	4020,
	4924,
	4051,
	4905,
	4885,
	4986,
	4986,
	4986,
	3996,
	4986,
	1741,
	1750,
	1042,
	4986,
	4020,
	4020,
	4986,
	4020,
	3996,
	4986,
	4814,
	4885,
	4986,
	4885,
	6750,
	6330,
	6420,
	6330,
	6329,
	4986,
	3996,
	4986,
	4814,
	4885,
	4986,
	4885,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	74,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
