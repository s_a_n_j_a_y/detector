﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Action`1<System.Collections.Generic.IList`1<BoundingBox>>
struct Action_1_t267C05F8874B612902659095FC16875EF00A902D;
// System.Action`1<UnityEngine.Color32[]>
struct Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9;
// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_tE8693FF0E67CDBA52BAFB211BFF1844D076ABAFB;
// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87;
// System.Action`1<Unity.Barracuda.Tensor>
struct Action_1_t626AB2FAB3033A14D62E8C4FF1A9C06528EC3390;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_tD7F74291FAA5A362F4A5F48D4E1CF3ED51FA6A33;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83;
// System.Func`2<System.ValueTuple`2<System.Int32,System.Single>,System.Single>
struct Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63;
// System.Func`2<System.Double,System.Single>
struct Func_2_t4C40E8255CD677D8102C7DF0D02FF06A36B97E3F;
// System.Func`2<System.Single,System.Double>
struct Func_2_t30CC93434BF7EFA66AF4592260CCDBC8BFC5DD87;
// System.Func`3<System.Single,System.Int32,System.ValueTuple`2<System.Int32,System.Single>>
struct Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266;
// System.Collections.Generic.IEnumerable`1<System.ValueTuple`2<System.Int32,System.Single>>
struct IEnumerable_1_t8DA27A0BF4DBF9C550C26661665C5E282C41DBF2;
// System.Collections.Generic.IEnumerable`1<System.Double>
struct IEnumerable_1_tAB7E6AAC5334AFEE42DB96DB8C245338F041A2DB;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t352FDDEA001ABE8E1D67849D2E2F3D1D75B03D41;
// System.Collections.Generic.IList`1<BoundingBox>
struct IList_1_t17DAD2D6103EE3FDBDEBD37ED7057619787280F2;
// System.Linq.IOrderedEnumerable`1<System.ValueTuple`2<System.Int32,System.Single>>
struct IOrderedEnumerable_1_tD469CB5C30D43EE230D8544DAF11D59789F7E5C3;
// System.Collections.Generic.List`1<Unity.Barracuda.Layer>
struct List_1_tFC347D5803110255687A796C13C6B7737E1FBCB5;
// System.Collections.Generic.List`1<System.String>
struct List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD;
// System.Collections.Generic.List`1<Unity.Barracuda.Model/ImporterWarning>
struct List_1_t56247FDC14896CB1245C68547A7F617F59F24DFE;
// System.Collections.Generic.List`1<Unity.Barracuda.Model/Input>
struct List_1_t86120430E6B82384E6CF3A073BDA4BEBB4F9993A;
// System.Collections.Generic.List`1<Unity.Barracuda.Model/Memory>
struct List_1_tE2D8EA34B9470D9A9FB201A6FF80EBB2D5393808;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4;
// UnityEngine.Color[]
struct ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389;
// UnityEngine.Color32[]
struct Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// System.Single[]
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
// System.ArgumentException
struct ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263;
// UnityEngine.UI.AspectRatioFitter
struct AspectRatioFitter_tEA1004A8240374C9BE7623981F999AA9789CDD09;
// UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C;
// BoundingBox
struct BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096;
// BoundingBoxDimensions
struct BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086;
// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860;
// CellDimensions
struct CellDimensions_tBEFCB6EBF0FC3F93784978F73375293DAFF08EAF;
// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// Detector
struct Detector_t4563441D0E64FB255B421A325E6916CB73C0A323;
// DimensionsBase
struct DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303;
// UnityEngine.UI.FontData
struct FontData_tB8E562846C6CB59C43260F69AE346B9BF3157224;
// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.Collections.IEnumerator
struct IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA;
// Unity.Barracuda.ITensorAllocator
struct ITensorAllocator_t553DDDB49ED44AEEF66ECF1C7CA6B532A76B0D74;
// Unity.Barracuda.ITensorData
struct ITensorData_t3A6F17AB959FF04A40F1CAF3FE450AA42855F8A2;
// Unity.Barracuda.IWorker
struct IWorker_t4B51670416E1CA4C2A1E15794C2ED9905D464555;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// UnityEngine.Mesh
struct Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// Unity.Barracuda.Model
struct Model_t8F5B69764BF210D112B40801428554C916789420;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// Unity.Barracuda.NNModel
struct NNModel_tE7CC55654A5FE992DAE3714CDB0A4711FB9A7153;
// Unity.Barracuda.NNModelData
struct NNModelData_tDB4FF13C94CAF2757FD7A137F2CF57710D629660;
// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A;
// PhoneCamera
struct PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4;
// UnityEngine.UI.RawImage
struct RawImage_tFF12F7DB574FBDC1863CF607C7A12A5D9F8D6179;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670;
// UnityEngine.RectTransform
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5;
// UnityEngine.RenderTexture
struct RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// System.String
struct String_t;
// Unity.Barracuda.Tensor
struct Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10;
// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62;
// UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC;
// UnityEngine.Texture
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700;
// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
// TFClassify.TextureTools
struct TextureTools_t4375AA99E42B9E4DE7491662E55DD0F502341F5F;
// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
// UnityEngine.Events.UnityAction
struct UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// UnityEngine.WebCamTexture
struct WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749;
// Detector/<>c
struct U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3;
// Detector/<>c__DisplayClass22_0
struct U3CU3Ec__DisplayClass22_0_t11D348A067DFC3081F35C9A414BC9626CDD04866;
// Detector/<Detect>d__18
struct U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8;
// PhoneCamera/<>c__DisplayClass14_0
struct U3CU3Ec__DisplayClass14_0_tFF5B9D2B76EB8F350A918BA71C6925E798589AA8;
// PhoneCamera/<ProcessImage>d__14
struct U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t3482EA130A01FF7EE2EEFE37F66A5215D08CFE24;
// TFClassify.TextureTools/<CropSquare>d__2
struct U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F;
// Unity.Barracuda.UniqueResourceId/UniqueResourceHelper
struct UniqueResourceHelper_t59DA06BEC5A93A2BD76CC262B47B9DBD6B261849;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t267C05F8874B612902659095FC16875EF00A902D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_tD7F74291FAA5A362F4A5F48D4E1CF3ED51FA6A33_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t30CC93434BF7EFA66AF4592260CCDBC8BFC5DD87_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t4C40E8255CD677D8102C7DF0D02FF06A36B97E3F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IWorker_t4B51670416E1CA4C2A1E15794C2ED9905D464555_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass14_0_tFF5B9D2B76EB8F350A918BA71C6925E798589AA8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass22_0_t11D348A067DFC3081F35C9A414BC9626CDD04866_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0FF1A933F6BC927FBAD982850B394C6D44B3F3F2;
IL2CPP_EXTERN_C String_t* _stringLiteral141DFBA76E0AF13B947128EAEE3242F482BBE356;
IL2CPP_EXTERN_C String_t* _stringLiteral142E7F04AD15522E43403C64A524E017226383F5;
IL2CPP_EXTERN_C String_t* _stringLiteral18345A1BE3A027B3306C286DC6C40328CFFF3DAE;
IL2CPP_EXTERN_C String_t* _stringLiteral1851E5597ABA51CECE59A7F352CB76C7D75DAF9D;
IL2CPP_EXTERN_C String_t* _stringLiteral1FC4D8FB742A77362133C567770CFD4F762AAEC7;
IL2CPP_EXTERN_C String_t* _stringLiteral29CEE6C774C2A6B13349634187AB40A780AAC0C0;
IL2CPP_EXTERN_C String_t* _stringLiteral3614C1A34A3BD65AE2BB7EF403804AA15E305D3F;
IL2CPP_EXTERN_C String_t* _stringLiteral3E9672DA3C80A70E01B048EBA83E47A9CC57CB08;
IL2CPP_EXTERN_C String_t* _stringLiteral58996B06CA854E96768D85CEA7C32D4965B0C68C;
IL2CPP_EXTERN_C String_t* _stringLiteral5AD43F7D251766D7095E126E2046470EEF1FE594;
IL2CPP_EXTERN_C String_t* _stringLiteral67086B015EBA786480C6ECD1FC5F51EDB80A5A0E;
IL2CPP_EXTERN_C String_t* _stringLiteral78622C01FE15EA71654ECF91AA6E911E2B4BECA1;
IL2CPP_EXTERN_C String_t* _stringLiteral840F32851B594336072216EBF80BA745E8F18A4D;
IL2CPP_EXTERN_C String_t* _stringLiteral9053C7BE1B5533FB96F3DE70C35EA0FFAB4FE19E;
IL2CPP_EXTERN_C String_t* _stringLiteral93B98C3040BF26B2D68E7DD514A4EE37837430F3;
IL2CPP_EXTERN_C String_t* _stringLiteral94E291225930DD4FDD61CA46BA3323392AF4D07C;
IL2CPP_EXTERN_C String_t* _stringLiteral9A7F9DE0C8F3D56FCC7CD55AC4B41D661174ABE2;
IL2CPP_EXTERN_C String_t* _stringLiteralB994FC3AD57A7150DAF257DF07AB5DB6E6BEFE04;
IL2CPP_EXTERN_C String_t* _stringLiteralBC9A54DB61539D95B06AD135584A19F1C3A21E80;
IL2CPP_EXTERN_C String_t* _stringLiteralC988FC027F8EF913F6D5AF5335C7D6DEAF47584B;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralDCBB48108EF25E2866BD80E6F7E7A1859DAFC8EB;
IL2CPP_EXTERN_C String_t* _stringLiteralDE7C2CB5991EEEC83B8129FF035C6C8425992CC6;
IL2CPP_EXTERN_C String_t* _stringLiteralEC47AE1BED75BF8A740DADA47F96B6270D14637C;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_First_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_m8CC5D68086B010D8F0DE7BC315C75B2F2244EEC5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_OrderByDescending_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mE32646C7E23945034DDC893CFDD067B739F2188A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m963F5737975A51BD55277B738111AA7E3EDD66AF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_mBFE23CA2A88CF76FCB150F230F156D046097B9CB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_m723EFB342860B437724ECE366144EB550DEC1E73_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mF0AED1993946FF775115C47514B29636472220A1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PhoneCamera_U3CTFDetectU3Eb__13_0_m8C397BA0EB24746D6967E6F04C682C86B540BD48_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PhoneCamera_U3CTFDetectU3Eb__13_1_mAD506FF440893CFFD070C52A1CBBDFF359DB3F51_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CCropSquareU3Ed__2_MoveNext_mC58C96CCC338EA44F5D3BA39E81E547C28E15094_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CCropSquareU3Ed__2_System_Collections_IEnumerator_Reset_m08EFD2ED4457D7E2CCF13F35B9204722A3493FDB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CDetectU3Ed__18_System_Collections_IEnumerator_Reset_m20841F6BEFEF1B50F595EE9CAC1373A72B6EAC0B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CProcessImageU3Ed__14_System_Collections_IEnumerator_Reset_mF3E80C2683A1452A45277CD2FA554C0AE7CC6B89_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CGetTopResultU3Eb__25_0_m6BF281AB15D6FA7FEAF74F680602ED2A2EA4781D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CGetTopResultU3Eb__25_1_m8954BE50605939235DCDA5D794EE0A7245021E74_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass14_0_U3CProcessImageU3Eb__0_m3E7C0BDCC16F0E5A81D725A7A4547CE8DE688FB5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass22_0_U3CSoftmaxU3Eb__0_m77CBFD936F493D01E896B0E518BC436EE3B3845E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass22_0_U3CSoftmaxU3Eb__1_mE9F6EA14222CF19AF3BDA5217A977C5BAEAFE5DF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ValueTuple_2__ctor_m8AD4450A363A5626EE8665BFDFA6B773D05EE581_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389;
struct Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259;
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tBB65183F1134474D09FF49B95625D25472B9BA8B 
{
};
struct Il2CppArrayBounds;

// BoundingBox
struct BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096  : public RuntimeObject
{
	// BoundingBoxDimensions BoundingBox::<Dimensions>k__BackingField
	BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* ___U3CDimensionsU3Ek__BackingField_0;
	// System.String BoundingBox::<Label>k__BackingField
	String_t* ___U3CLabelU3Ek__BackingField_1;
	// System.Single BoundingBox::<Confidence>k__BackingField
	float ___U3CConfidenceU3Ek__BackingField_2;
};

// DimensionsBase
struct DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303  : public RuntimeObject
{
	// System.Single DimensionsBase::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_0;
	// System.Single DimensionsBase::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_1;
	// System.Single DimensionsBase::<Height>k__BackingField
	float ___U3CHeightU3Ek__BackingField_2;
	// System.Single DimensionsBase::<Width>k__BackingField
	float ___U3CWidthU3Ek__BackingField_3;
};

// Unity.Barracuda.Model
struct Model_t8F5B69764BF210D112B40801428554C916789420  : public RuntimeObject
{
	// System.String Unity.Barracuda.Model::layout
	String_t* ___layout_4;
	// System.Collections.Generic.List`1<Unity.Barracuda.Model/Input> Unity.Barracuda.Model::inputs
	List_1_t86120430E6B82384E6CF3A073BDA4BEBB4F9993A* ___inputs_5;
	// System.Collections.Generic.List`1<System.String> Unity.Barracuda.Model::outputs
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___outputs_6;
	// System.Collections.Generic.List`1<Unity.Barracuda.Model/Memory> Unity.Barracuda.Model::memories
	List_1_tE2D8EA34B9470D9A9FB201A6FF80EBB2D5393808* ___memories_7;
	// System.Collections.Generic.List`1<Unity.Barracuda.Layer> Unity.Barracuda.Model::layers
	List_1_tFC347D5803110255687A796C13C6B7737E1FBCB5* ___layers_8;
	// System.String Unity.Barracuda.Model::IrSource
	String_t* ___IrSource_9;
	// System.String Unity.Barracuda.Model::IrVersion
	String_t* ___IrVersion_10;
	// System.String Unity.Barracuda.Model::ProducerName
	String_t* ___ProducerName_11;
	// System.Collections.Generic.List`1<Unity.Barracuda.Model/ImporterWarning> Unity.Barracuda.Model::<Warnings>k__BackingField
	List_1_t56247FDC14896CB1245C68547A7F617F59F24DFE* ___U3CWarningsU3Ek__BackingField_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Unity.Barracuda.Model::<Metadata>k__BackingField
	Dictionary_2_t46B2DB028096FA2B828359E52F37F3105A83AD83* ___U3CMetadataU3Ek__BackingField_13;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// TFClassify.TextureTools
struct TextureTools_t4375AA99E42B9E4DE7491662E55DD0F502341F5F  : public RuntimeObject
{
};

// Unity.Barracuda.UniqueResourceId
struct UniqueResourceId_t3A31ABD6FA0521225014929D3039C136EBEE965E  : public RuntimeObject
{
	// System.Int32 Unity.Barracuda.UniqueResourceId::<uniqueId>k__BackingField
	int32_t ___U3CuniqueIdU3Ek__BackingField_1;
};

struct UniqueResourceId_t3A31ABD6FA0521225014929D3039C136EBEE965E_StaticFields
{
	// Unity.Barracuda.UniqueResourceId/UniqueResourceHelper Unity.Barracuda.UniqueResourceId::SpinLock
	UniqueResourceHelper_t59DA06BEC5A93A2BD76CC262B47B9DBD6B261849* ___SpinLock_0;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
};

// Detector/<>c
struct U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3  : public RuntimeObject
{
};

struct U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_StaticFields
{
	// Detector/<>c Detector/<>c::<>9
	U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3* ___U3CU3E9_0;
	// System.Func`3<System.Single,System.Int32,System.ValueTuple`2<System.Int32,System.Single>> Detector/<>c::<>9__25_0
	Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266* ___U3CU3E9__25_0_1;
	// System.Func`2<System.ValueTuple`2<System.Int32,System.Single>,System.Single> Detector/<>c::<>9__25_1
	Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63* ___U3CU3E9__25_1_2;
};

// Detector/<>c__DisplayClass22_0
struct U3CU3Ec__DisplayClass22_0_t11D348A067DFC3081F35C9A414BC9626CDD04866  : public RuntimeObject
{
	// System.Single Detector/<>c__DisplayClass22_0::maxVal
	float ___maxVal_0;
	// System.Double Detector/<>c__DisplayClass22_0::sumExp
	double ___sumExp_1;
};

// Detector/<Detect>d__18
struct U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4  : public RuntimeObject
{
	// System.Int32 Detector/<Detect>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Detector/<Detect>d__18::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// UnityEngine.Color32[] Detector/<Detect>d__18::picture
	Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___picture_2;
	// Detector Detector/<Detect>d__18::<>4__this
	Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* ___U3CU3E4__this_3;
	// System.Action`1<System.Collections.Generic.IList`1<BoundingBox>> Detector/<Detect>d__18::callback
	Action_1_t267C05F8874B612902659095FC16875EF00A902D* ___callback_4;
	// Unity.Barracuda.Tensor Detector/<Detect>d__18::<tensor>5__2
	Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* ___U3CtensorU3E5__2_5;
};

// PhoneCamera/<>c__DisplayClass14_0
struct U3CU3Ec__DisplayClass14_0_tFF5B9D2B76EB8F350A918BA71C6925E798589AA8  : public RuntimeObject
{
	// PhoneCamera PhoneCamera/<>c__DisplayClass14_0::<>4__this
	PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* ___U3CU3E4__this_0;
	// System.Int32 PhoneCamera/<>c__DisplayClass14_0::inputSize
	int32_t ___inputSize_1;
	// System.Action`1<UnityEngine.Color32[]> PhoneCamera/<>c__DisplayClass14_0::callback
	Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9* ___callback_2;
};

// PhoneCamera/<ProcessImage>d__14
struct U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3  : public RuntimeObject
{
	// System.Int32 PhoneCamera/<ProcessImage>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PhoneCamera/<ProcessImage>d__14::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// PhoneCamera PhoneCamera/<ProcessImage>d__14::<>4__this
	PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* ___U3CU3E4__this_2;
	// System.Int32 PhoneCamera/<ProcessImage>d__14::inputSize
	int32_t ___inputSize_3;
	// System.Action`1<UnityEngine.Color32[]> PhoneCamera/<ProcessImage>d__14::callback
	Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9* ___callback_4;
};

// TFClassify.TextureTools/<CropSquare>d__2
struct U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F  : public RuntimeObject
{
	// System.Int32 TFClassify.TextureTools/<CropSquare>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TFClassify.TextureTools/<CropSquare>d__2::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// UnityEngine.WebCamTexture TFClassify.TextureTools/<CropSquare>d__2::texture
	WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* ___texture_2;
	// TFClassify.TextureTools/RectOptions TFClassify.TextureTools/<CropSquare>d__2::rectOptions
	int32_t ___rectOptions_3;
	// System.Action`1<UnityEngine.Texture2D> TFClassify.TextureTools/<CropSquare>d__2::callback
	Action_1_tD7F74291FAA5A362F4A5F48D4E1CF3ED51FA6A33* ___callback_4;
	// UnityEngine.Texture2D TFClassify.TextureTools/<CropSquare>d__2::<result>5__2
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___U3CresultU3E5__2_5;
};

// System.ValueTuple`2<System.Int32,System.Single>
struct ValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69 
{
	// T1 System.ValueTuple`2::Item1
	int32_t ___Item1_0;
	// T2 System.ValueTuple`2::Item2
	float ___Item2_1;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// BoundingBoxDimensions
struct BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086  : public DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303
{
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// CellDimensions
struct CellDimensions_tBEFCB6EBF0FC3F93784978F73375293DAFF08EAF  : public DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303
{
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// UnityEngine.Color32
struct Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};
};

// System.Double
struct Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F 
{
	// System.Double System.Double::m_value
	double ___m_value_0;
};

// UnityEngine.DrivenRectTransformTracker
struct DrivenRectTransformTracker_tFB0706C933E3C68E4F377C204FCEEF091F1EE0B1 
{
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_tFB0706C933E3C68E4F377C204FCEEF091F1EE0B1__padding[1];
	};
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// UnityEngine.Rect
struct Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D 
{
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// Unity.Barracuda.TensorShape
struct TensorShape_t47FF7A9460E8AE4CADC6FFA19814C53203DD16E3 
{
	// System.Int32 Unity.Barracuda.TensorShape::d0
	int32_t ___d0_21;
	// System.Int32 Unity.Barracuda.TensorShape::d1
	int32_t ___d1_22;
	// System.Int32 Unity.Barracuda.TensorShape::d2
	int32_t ___d2_23;
	// System.Int32 Unity.Barracuda.TensorShape::d3
	int32_t ___d3_24;
	// System.Int32 Unity.Barracuda.TensorShape::d4
	int32_t ___d4_25;
	// System.Int32 Unity.Barracuda.TensorShape::d5
	int32_t ___d5_26;
	// System.Int32 Unity.Barracuda.TensorShape::d6
	int32_t ___d6_27;
	// System.Int32 Unity.Barracuda.TensorShape::d7
	int32_t ___d7_28;
	// Unity.Barracuda.TensorShape/NamedDimension Unity.Barracuda.TensorShape::m_UsesNamedDimensions
	uint8_t ___m_UsesNamedDimensions_29;
	// System.Int32 Unity.Barracuda.TensorShape::m_Rank
	int32_t ___m_Rank_30;
};

struct TensorShape_t47FF7A9460E8AE4CADC6FFA19814C53203DD16E3_StaticFields
{
	// System.Int32[] Unity.Barracuda.TensorShape::DataFeatures
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___DataFeatures_13;
	// System.Int32[] Unity.Barracuda.TensorShape::KernelSpatials
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___KernelSpatials_20;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_tE8693FF0E67CDBA52BAFB211BFF1844D076ABAFB* ___m_completeCallback_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};

// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};

struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// Unity.Barracuda.Tensor
struct Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10  : public UniqueResourceId_t3A31ABD6FA0521225014929D3039C136EBEE965E
{
	// Unity.Barracuda.DataType Unity.Barracuda.Tensor::m_preferredDataType
	int32_t ___m_preferredDataType_2;
	// Unity.Barracuda.ITensorData Unity.Barracuda.Tensor::m_TensorOnDevice
	RuntimeObject* ___m_TensorOnDevice_3;
	// Unity.Barracuda.ITensorAllocator Unity.Barracuda.Tensor::m_TensorAllocator
	RuntimeObject* ___m_TensorAllocator_4;
	// System.Single[] Unity.Barracuda.Tensor::m_Cache
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___m_Cache_5;
	// System.Boolean Unity.Barracuda.Tensor::m_CacheIsDirty
	bool ___m_CacheIsDirty_6;
	// System.Boolean Unity.Barracuda.Tensor::m_Disposed
	bool ___m_Disposed_7;
	// System.String Unity.Barracuda.Tensor::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_9;
	// Unity.Barracuda.TensorShape Unity.Barracuda.Tensor::<shape>k__BackingField
	TensorShape_t47FF7A9460E8AE4CADC6FFA19814C53203DD16E3 ___U3CshapeU3Ek__BackingField_10;
	// System.Boolean Unity.Barracuda.Tensor::m_Disposing
	bool ___m_Disposing_11;
};

struct Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10_StaticFields
{
	// System.Action`1<Unity.Barracuda.Tensor> Unity.Barracuda.Tensor::tensorDisposed
	Action_1_t626AB2FAB3033A14D62E8C4FF1A9C06528EC3390* ___tensorDisposed_8;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_pinvoke : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_com : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// UnityEngine.Texture
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700_StaticFields
{
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;
};

// System.Action`1<System.Collections.Generic.IList`1<BoundingBox>>
struct Action_1_t267C05F8874B612902659095FC16875EF00A902D  : public MulticastDelegate_t
{
};

// System.Action`1<UnityEngine.Color32[]>
struct Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9  : public MulticastDelegate_t
{
};

// System.Action`1<System.Object>
struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87  : public MulticastDelegate_t
{
};

// System.Action`1<UnityEngine.Texture2D>
struct Action_1_tD7F74291FAA5A362F4A5F48D4E1CF3ED51FA6A33  : public MulticastDelegate_t
{
};

// System.Func`2<System.ValueTuple`2<System.Int32,System.Single>,System.Single>
struct Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63  : public MulticastDelegate_t
{
};

// System.Func`2<System.Double,System.Single>
struct Func_2_t4C40E8255CD677D8102C7DF0D02FF06A36B97E3F  : public MulticastDelegate_t
{
};

// System.Func`2<System.Single,System.Double>
struct Func_2_t30CC93434BF7EFA66AF4592260CCDBC8BFC5DD87  : public MulticastDelegate_t
{
};

// System.Func`3<System.Single,System.Int32,System.ValueTuple`2<System.Int32,System.Single>>
struct Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266  : public MulticastDelegate_t
{
};

// System.ArgumentException
struct ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
	// System.String System.ArgumentException::_paramName
	String_t* ____paramName_18;
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// Unity.Barracuda.NNModel
struct NNModel_tE7CC55654A5FE992DAE3714CDB0A4711FB9A7153  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	// Unity.Barracuda.NNModelData Unity.Barracuda.NNModel::modelData
	NNModelData_tDB4FF13C94CAF2757FD7A137F2CF57710D629660* ___modelData_4;
	// Unity.Barracuda.Model Unity.Barracuda.NNModel::m_Model
	Model_t8F5B69764BF210D112B40801428554C916789420* ___m_Model_5;
	// System.Single Unity.Barracuda.NNModel::m_LastLoaded
	float ___m_LastLoaded_6;
};

// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// UnityEngine.RenderTexture
struct RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27  : public Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700
{
};

// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4  : public Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700
{
};

// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.WebCamTexture
struct WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749  : public Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.RectTransform
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5  : public Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1
{
};

struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_StaticFields
{
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t3482EA130A01FF7EE2EEFE37F66A5215D08CFE24* ___reapplyDrivenProperties_4;
};

// Detector
struct Detector_t4563441D0E64FB255B421A325E6916CB73C0A323  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Unity.Barracuda.NNModel Detector::modelFile
	NNModel_tE7CC55654A5FE992DAE3714CDB0A4711FB9A7153* ___modelFile_4;
	// UnityEngine.UI.Text Detector::_text
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ____text_5;
	// Unity.Barracuda.IWorker Detector::worker
	RuntimeObject* ___worker_12;
	// System.String[] Detector::labels
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___labels_20;
};

// PhoneCamera
struct PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single PhoneCamera::cameraScale
	float ___cameraScale_4;
	// System.Single PhoneCamera::shiftX
	float ___shiftX_5;
	// System.Single PhoneCamera::shiftY
	float ___shiftY_6;
	// System.Single PhoneCamera::scaleFactor
	float ___scaleFactor_7;
	// System.Boolean PhoneCamera::camAvailable
	bool ___camAvailable_8;
	// UnityEngine.WebCamTexture PhoneCamera::backCamera
	WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* ___backCamera_9;
	// System.Boolean PhoneCamera::isWorking
	bool ___isWorking_10;
	// Detector PhoneCamera::detector
	Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* ___detector_11;
	// UnityEngine.UI.RawImage PhoneCamera::background
	RawImage_tFF12F7DB574FBDC1863CF607C7A12A5D9F8D6179* ___background_12;
	// UnityEngine.UI.AspectRatioFitter PhoneCamera::fitter
	AspectRatioFitter_tEA1004A8240374C9BE7623981F999AA9789CDD09* ___fitter_13;
};

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// UnityEngine.UI.AspectRatioFitter
struct AspectRatioFitter_tEA1004A8240374C9BE7623981F999AA9789CDD09  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
	// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::m_AspectMode
	int32_t ___m_AspectMode_4;
	// System.Single UnityEngine.UI.AspectRatioFitter::m_AspectRatio
	float ___m_AspectRatio_5;
	// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::m_Rect
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___m_Rect_6;
	// System.Boolean UnityEngine.UI.AspectRatioFitter::m_DelayedSetDirty
	bool ___m_DelayedSetDirty_7;
	// System.Boolean UnityEngine.UI.AspectRatioFitter::m_DoesParentExist
	bool ___m_DoesParentExist_8;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.AspectRatioFitter::m_Tracker
	DrivenRectTransformTracker_tFB0706C933E3C68E4F377C204FCEEF091F1EE0B1 ___m_Tracker_9;
};

// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTargetCache
	bool ___m_RaycastTargetCache_11;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___m_RaycastPadding_12;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___m_RectTransform_13;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860* ___m_CanvasRenderer_14;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* ___m_Canvas_15;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_16;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyLayoutCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyVertsCallback_19;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyMaterialCallback_20;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___m_CachedMesh_23;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___m_CachedUvs_24;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4* ___m_ColorTweenRunner_25;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_26;
};

struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___s_Mesh_21;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE* ___s_VertexHelper_22;
};

// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E  : public Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931
{
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_27;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_MaskMaterial_28;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670* ___m_ParentMask_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_31;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_32;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8* ___m_OnCullStateChanged_33;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_34;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_35;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___m_Corners_36;
};

// UnityEngine.UI.RawImage
struct RawImage_tFF12F7DB574FBDC1863CF607C7A12A5D9F8D6179  : public MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E
{
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___m_Texture_37;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___m_UVRect_38;
};

// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62  : public MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E
{
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_tB8E562846C6CB59C43260F69AE346B9BF3157224* ___m_FontData_37;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_TextCache_39;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_TextCacheForLayout_40;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_42;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F* ___m_TempVerts_43;
};

struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_DefaultText_41;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Color32[]
struct Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259  : public RuntimeArray
{
	ALIGN_FIELD (8) Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B m_Items[1];

	inline Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B value)
	{
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C  : public RuntimeArray
{
	ALIGN_FIELD (8) float m_Items[1];

	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389  : public RuntimeArray
{
	ALIGN_FIELD (8) Color_tD001788D726C3A7F1379BEED0260B9591F440C1F m_Items[1];

	inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Func`2<System.Single,System.Double>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m5A0602CB85D5CD42F7B0529A5C0435342FD0001F_gshared (Func_2_t30CC93434BF7EFA66AF4592260CCDBC8BFC5DD87* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<System.Single,System.Double>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_mBFE23CA2A88CF76FCB150F230F156D046097B9CB_gshared (RuntimeObject* ___source0, Func_2_t30CC93434BF7EFA66AF4592260CCDBC8BFC5DD87* ___selector1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Double,System.Single>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m8B3A2701A41C48403E8F3F0B902AD81B31F18A67_gshared (Func_2_t4C40E8255CD677D8102C7DF0D02FF06A36B97E3F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<System.Double,System.Single>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m963F5737975A51BD55277B738111AA7E3EDD66AF_gshared (RuntimeObject* ___source0, Func_2_t4C40E8255CD677D8102C7DF0D02FF06A36B97E3F* ___selector1, const RuntimeMethod* method) ;
// TSource[] System.Linq.Enumerable::ToArray<System.Single>(System.Collections.Generic.IEnumerable`1<TSource>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* Enumerable_ToArray_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mF0AED1993946FF775115C47514B29636472220A1_gshared (RuntimeObject* ___source0, const RuntimeMethod* method) ;
// System.Void System.Func`3<System.Single,System.Int32,System.ValueTuple`2<System.Int32,System.Single>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_3__ctor_m66045AE411F68F153ADA7C2D06A34602ADB1E316_gshared (Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<System.Single,System.ValueTuple`2<System.Int32,System.Single>>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,System.Int32,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_m723EFB342860B437724ECE366144EB550DEC1E73_gshared (RuntimeObject* ___source0, Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266* ___selector1, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.ValueTuple`2<System.Int32,System.Single>,System.Single>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mAC4A9C9324857AE713A728A56F2729F6430F48A2_gshared (Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderByDescending<System.ValueTuple`2<System.Int32,System.Single>,System.Single>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_OrderByDescending_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mE32646C7E23945034DDC893CFDD067B739F2188A_gshared (RuntimeObject* ___source0, Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63* ___keySelector1, const RuntimeMethod* method) ;
// TSource System.Linq.Enumerable::First<System.ValueTuple`2<System.Int32,System.Single>>(System.Collections.Generic.IEnumerable`1<TSource>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69 Enumerable_First_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_m8CC5D68086B010D8F0DE7BC315C75B2F2244EEC5_gshared (RuntimeObject* ___source0, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Object>::Invoke(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___obj0, const RuntimeMethod* method) ;
// System.Void System.ValueTuple`2<System.Int32,System.Single>::.ctor(T1,T2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValueTuple_2__ctor_m8AD4450A363A5626EE8665BFDFA6B773D05EE581_gshared (ValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69* __this, int32_t ___item10, float ___item21, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_m2E1DFA67718FC1A0B6E5DFEB78831FFE9C059EB4_gshared (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;

// Unity.Barracuda.Model Unity.Barracuda.ModelLoader::Load(Unity.Barracuda.NNModel,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Model_t8F5B69764BF210D112B40801428554C916789420* ModelLoader_Load_mDADBCE3F4F7F98CECFD6531B7C752ABAFBD8F0C0 (NNModel_tE7CC55654A5FE992DAE3714CDB0A4711FB9A7153* ___model0, bool ___verbose1, bool ___skipWeights2, const RuntimeMethod* method) ;
// Unity.Barracuda.IWorker Unity.Barracuda.WorkerFactory::CreateWorker(Unity.Barracuda.Model,Unity.Barracuda.WorkerFactory/Device,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WorkerFactory_CreateWorker_mD9C277F1AC92D6532A85E84C5CABFD0C35509D88 (Model_t8F5B69764BF210D112B40801428554C916789420* ___model0, int32_t ___device1, bool ___verbose2, const RuntimeMethod* method) ;
// System.Void Detector/<Detect>d__18::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDetectU3Ed__18__ctor_mDC24DE8F8D42EAEBF097B384F1D425CBFEDC01DA (U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void Unity.Barracuda.Tensor::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Single[],System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tensor__ctor_m6F4A668FB0CD6ACD79447138D103F9445894BDDA (Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* __this, int32_t ___n0, int32_t ___h1, int32_t ___w2, int32_t ___c3, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___srcData4, String_t* ___name5, const RuntimeMethod* method) ;
// System.Single Detector::GetConfidence(Unity.Barracuda.Tensor,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Detector_GetConfidence_mD04080750424C52D2710A6385C9A80FAC64C54AE (Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* __this, Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* ___modelOutput0, int32_t ___x1, int32_t ___y2, int32_t ___channel3, const RuntimeMethod* method) ;
// System.Single[] Detector::ExtractClasses(Unity.Barracuda.Tensor,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* Detector_ExtractClasses_mBE2BC473855937D06B745973F3745B8A51299806 (Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* __this, Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* ___modelOutput0, int32_t ___x1, int32_t ___y2, int32_t ___channel3, const RuntimeMethod* method) ;
// System.ValueTuple`2<System.Int32,System.Single> Detector::GetTopResult(System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69 Detector_GetTopResult_m77F06CCB4F2570BD7D8F9A8E5877108CEA0B610B (Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* __this, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___predictedClasses0, const RuntimeMethod* method) ;
// System.Void Detector/<>c__DisplayClass22_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass22_0__ctor_m31724DA5864B4C47CF318E8E1956EAF7D971E383 (U3CU3Ec__DisplayClass22_0_t11D348A067DFC3081F35C9A414BC9626CDD04866* __this, const RuntimeMethod* method) ;
// System.Single System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Enumerable_Max_m3215FA2A65ACAFE226A0FBE065D772C7CE1A5E9F (RuntimeObject* ___source0, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Single,System.Double>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m5A0602CB85D5CD42F7B0529A5C0435342FD0001F (Func_2_t30CC93434BF7EFA66AF4592260CCDBC8BFC5DD87* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t30CC93434BF7EFA66AF4592260CCDBC8BFC5DD87*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m5A0602CB85D5CD42F7B0529A5C0435342FD0001F_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<System.Single,System.Double>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
inline RuntimeObject* Enumerable_Select_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_mBFE23CA2A88CF76FCB150F230F156D046097B9CB (RuntimeObject* ___source0, Func_2_t30CC93434BF7EFA66AF4592260CCDBC8BFC5DD87* ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t30CC93434BF7EFA66AF4592260CCDBC8BFC5DD87*, const RuntimeMethod*))Enumerable_Select_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_mBFE23CA2A88CF76FCB150F230F156D046097B9CB_gshared)(___source0, ___selector1, method);
}
// System.Double System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Double>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double Enumerable_Sum_mB4B29B0D6E567EB810D0B439945F9BC6ACC01284 (RuntimeObject* ___source0, const RuntimeMethod* method) ;
// System.Void System.Func`2<System.Double,System.Single>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m8B3A2701A41C48403E8F3F0B902AD81B31F18A67 (Func_2_t4C40E8255CD677D8102C7DF0D02FF06A36B97E3F* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t4C40E8255CD677D8102C7DF0D02FF06A36B97E3F*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_m8B3A2701A41C48403E8F3F0B902AD81B31F18A67_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<System.Double,System.Single>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
inline RuntimeObject* Enumerable_Select_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m963F5737975A51BD55277B738111AA7E3EDD66AF (RuntimeObject* ___source0, Func_2_t4C40E8255CD677D8102C7DF0D02FF06A36B97E3F* ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t4C40E8255CD677D8102C7DF0D02FF06A36B97E3F*, const RuntimeMethod*))Enumerable_Select_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m963F5737975A51BD55277B738111AA7E3EDD66AF_gshared)(___source0, ___selector1, method);
}
// TSource[] System.Linq.Enumerable::ToArray<System.Single>(System.Collections.Generic.IEnumerable`1<TSource>)
inline SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* Enumerable_ToArray_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mF0AED1993946FF775115C47514B29636472220A1 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mF0AED1993946FF775115C47514B29636472220A1_gshared)(___source0, method);
}
// System.Single Unity.Barracuda.Tensor::get_Item(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Tensor_get_Item_mA53C387CBDB9F0EA0D069DD7BFC45970172290C5 (Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* __this, int32_t ___b0, int32_t ___h1, int32_t ___w2, int32_t ___ch3, const RuntimeMethod* method) ;
// System.Single Detector::Sigmoid(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Detector_Sigmoid_mCF05C6BD26BEC3884F205912F1BD56865E3B28D0 (Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* __this, float ___value0, const RuntimeMethod* method) ;
// System.Single[] Detector::Softmax(System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* Detector_Softmax_mC92F5060627D870E16B67ABE6FE364F25D2C16CA (Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* __this, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___values0, const RuntimeMethod* method) ;
// System.Void System.Func`3<System.Single,System.Int32,System.ValueTuple`2<System.Int32,System.Single>>::.ctor(System.Object,System.IntPtr)
inline void Func_3__ctor_m66045AE411F68F153ADA7C2D06A34602ADB1E316 (Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_3__ctor_m66045AE411F68F153ADA7C2D06A34602ADB1E316_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select<System.Single,System.ValueTuple`2<System.Int32,System.Single>>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,System.Int32,TResult>)
inline RuntimeObject* Enumerable_Select_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_m723EFB342860B437724ECE366144EB550DEC1E73 (RuntimeObject* ___source0, Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266* ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266*, const RuntimeMethod*))Enumerable_Select_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_m723EFB342860B437724ECE366144EB550DEC1E73_gshared)(___source0, ___selector1, method);
}
// System.Void System.Func`2<System.ValueTuple`2<System.Int32,System.Single>,System.Single>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mAC4A9C9324857AE713A728A56F2729F6430F48A2 (Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63*, RuntimeObject*, intptr_t, const RuntimeMethod*))Func_2__ctor_mAC4A9C9324857AE713A728A56F2729F6430F48A2_gshared)(__this, ___object0, ___method1, method);
}
// System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderByDescending<System.ValueTuple`2<System.Int32,System.Single>,System.Single>(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
inline RuntimeObject* Enumerable_OrderByDescending_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mE32646C7E23945034DDC893CFDD067B739F2188A (RuntimeObject* ___source0, Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63* ___keySelector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63*, const RuntimeMethod*))Enumerable_OrderByDescending_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mE32646C7E23945034DDC893CFDD067B739F2188A_gshared)(___source0, ___keySelector1, method);
}
// TSource System.Linq.Enumerable::First<System.ValueTuple`2<System.Int32,System.Single>>(System.Collections.Generic.IEnumerable`1<TSource>)
inline ValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69 Enumerable_First_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_m8CC5D68086B010D8F0DE7BC315C75B2F2244EEC5 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  ValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69 (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_First_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_m8CC5D68086B010D8F0DE7BC315C75B2F2244EEC5_gshared)(___source0, method);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void Detector/<Detect>d__18::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDetectU3Ed__18_U3CU3Em__Finally1_m21985138C6A5A3A76754E0480D83BA0AA29FAA94 (U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4* __this, const RuntimeMethod* method) ;
// System.Void Detector/<Detect>d__18::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDetectU3Ed__18_System_IDisposable_Dispose_m612F41816C95EE746B09DFF12C97156E88C27218 (U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4* __this, const RuntimeMethod* method) ;
// Unity.Barracuda.Tensor Detector::TransformInput(UnityEngine.Color32[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* Detector_TransformInput_m70DB7B82E6F85F933AEBB34D98CCD62F67C12116 (Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___pic0, int32_t ___width1, int32_t ___height2, const RuntimeMethod* method) ;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812 (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, RuntimeObject* ___routine0, const RuntimeMethod* method) ;
// System.Collections.Generic.IList`1<BoundingBox> Detector::ParseOutputs(Unity.Barracuda.Tensor,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Detector_ParseOutputs_m43D3C60ECD5961E7398A77E019855B701EA97D05 (Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* __this, Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* ___yoloModelOutput0, float ___threshold1, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Collections.Generic.IList`1<BoundingBox>>::Invoke(T)
inline void Action_1_Invoke_mFC32950FF57D2118CA6414DEED7119D840B46E21_inline (Action_1_t267C05F8874B612902659095FC16875EF00A902D* __this, RuntimeObject* ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t267C05F8874B612902659095FC16875EF00A902D*, RuntimeObject*, const RuntimeMethod*))Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline)(__this, ___obj0, method);
}
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* __this, const RuntimeMethod* method) ;
// System.Void Detector/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m67170BF871ACE0B683BA53B3E1B6F6104C866FD8 (U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3* __this, const RuntimeMethod* method) ;
// System.Void System.ValueTuple`2<System.Int32,System.Single>::.ctor(T1,T2)
inline void ValueTuple_2__ctor_m8AD4450A363A5626EE8665BFDFA6B773D05EE581 (ValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69* __this, int32_t ___item10, float ___item21, const RuntimeMethod* method)
{
	((  void (*) (ValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69*, int32_t, float, const RuntimeMethod*))ValueTuple_2__ctor_m8AD4450A363A5626EE8665BFDFA6B773D05EE581_gshared)(__this, ___item10, ___item21, method);
}
// System.Void DimensionsBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DimensionsBase__ctor_m9AFA3033ED5147C13EC158B85DC75513599B0585 (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, const RuntimeMethod* method) ;
// BoundingBoxDimensions BoundingBox::get_Dimensions()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* BoundingBox_get_Dimensions_m4F3FC84E34A2F040280CA9119B6AE2C9222F55AC_inline (BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096* __this, const RuntimeMethod* method) ;
// System.Single DimensionsBase::get_X()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float DimensionsBase_get_X_m2AA39E00503C51C587F473D1ADBEC9CCB5D54F41_inline (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, const RuntimeMethod* method) ;
// System.Single DimensionsBase::get_Y()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float DimensionsBase_get_Y_m170190F8D1531F544A40AB55817C1EC7EFD8A864_inline (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, const RuntimeMethod* method) ;
// System.Single DimensionsBase::get_Width()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float DimensionsBase_get_Width_m6B20834F813A905E59F902C802FAB97D21286ECF_inline (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, const RuntimeMethod* method) ;
// System.Single DimensionsBase::get_Height()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float DimensionsBase_get_Height_m6632C66BBCA8F5D5ABC7E95739DD3713DBA7E9BE_inline (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m18C3033D135097BEE424AAA68D91C706D2647F23 (Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D* __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method) ;
// System.String BoundingBox::get_Label()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* BoundingBox_get_Label_m6C86B30C4199D01AA9838287FEA1580EA9C3DA2D_inline (BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096* __this, const RuntimeMethod* method) ;
// System.Single BoundingBox::get_Confidence()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float BoundingBox_get_Confidence_m7930AB5EF72D84E6FCFB1432F5F62F169BB1C3A2_inline (BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096* __this, const RuntimeMethod* method) ;
// System.String System.String::Format(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m918500C1EFB475181349A79989BB79BB36102894 (String_t* ___format0, ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___args1, const RuntimeMethod* method) ;
// System.Void UnityEngine.WebCamTexture::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebCamTexture__ctor_mCDCF373E41263EE323147C4A50609EBE9FA28269 (WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.UI.RawImage::set_texture(UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RawImage_set_texture_mC016318C95CC17A826D57DD219DBCB6DFD295C02 (RawImage_tFF12F7DB574FBDC1863CF607C7A12A5D9F8D6179* __this, Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.WebCamTexture::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebCamTexture_Play_mAB313C6F98D5433C414DA31DD96316BDE8D19A26 (WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* __this, const RuntimeMethod* method) ;
// System.Void PhoneCamera::CalculateShift(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhoneCamera_CalculateShift_mB728F0C3229A2001D59AE21D5EF675726F23F608 (PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* __this, int32_t ___inputSize0, const RuntimeMethod* method) ;
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectRatio(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspectRatioFitter_set_aspectRatio_m4192E203648BE0ACA39D9C0540C982331CEA91D9 (AspectRatioFitter_tEA1004A8240374C9BE7623981F999AA9789CDD09* __this, float ___value0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.WebCamTexture::get_videoVerticallyMirrored()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebCamTexture_get_videoVerticallyMirrored_mDC7525B796A2629927EF113DA199DDE200B1B52A (WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* __this, const RuntimeMethod* method) ;
// UnityEngine.RectTransform UnityEngine.UI.Graphic::get_rectTransform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* Graphic_get_rectTransform_mF4752E8934267D630810E84CE02CDFB81EB1FD6D (Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mBA79E811BAF6C47B80FF76414C12B47B3CD03633 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.WebCamTexture::get_videoRotationAngle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WebCamTexture_get_videoRotationAngle_m2BF420A1243F56415BEF82CC84AB4C7B342C991F (WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localEulerAngles_m0458551662A1A51FDCA4C0417282B25D391661DF (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_mF608FF3252213E7EFA1F0D2F744C28110E9E5AC9 (const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_m01A3102DE71EE1FBEA51D09D6B0261CF864FE8F9 (const RuntimeMethod* method) ;
// System.Void PhoneCamera::TFDetect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhoneCamera_TFDetect_mFF76488480372773AF1F894433E4DAC1FFCB8E80 (PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* __this, const RuntimeMethod* method) ;
// System.Void System.Action`1<UnityEngine.Color32[]>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m9A437718C386CE2D3AC7049893F4C00BA6CDE990 (Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_1__ctor_m2E1DFA67718FC1A0B6E5DFEB78831FFE9C059EB4_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.IEnumerator PhoneCamera::ProcessImage(System.Int32,System.Action`1<UnityEngine.Color32[]>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PhoneCamera_ProcessImage_m68C34D4C3E9C403AE677F921D15E7B5D06EF650E (PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* __this, int32_t ___inputSize0, Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9* ___callback1, const RuntimeMethod* method) ;
// System.Void PhoneCamera/<ProcessImage>d__14::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessImageU3Ed__14__ctor_m64F65AE69E02E4D6DA72E4A43FD9E2E4D712C584 (U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// UnityEngine.Texture2D TFClassify.TextureTools::scaled(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.FilterMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* TextureTools_scaled_m42BE122AF1DC9EBC821B957472F4E956F8395885 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___src0, int32_t ___width1, int32_t ___height2, int32_t ___mode3, const RuntimeMethod* method) ;
// UnityEngine.Color32[] TFClassify.TextureTools::RotateImageMatrix(UnityEngine.Color32[],System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* TextureTools_RotateImageMatrix_m6042CB8FC50C973D9A2D1D1DBE53E21435BF073B (Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___matrix0, int32_t ___width1, int32_t ___height2, int32_t ___angle3, const RuntimeMethod* method) ;
// System.Void System.Action`1<System.Collections.Generic.IList`1<BoundingBox>>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mE843E93E00C081B2E44D3AFCF595129033F8FA64 (Action_1_t267C05F8874B612902659095FC16875EF00A902D* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t267C05F8874B612902659095FC16875EF00A902D*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_1__ctor_m2E1DFA67718FC1A0B6E5DFEB78831FFE9C059EB4_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.IEnumerator Detector::Detect(UnityEngine.Color32[],System.Action`1<System.Collections.Generic.IList`1<BoundingBox>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Detector_Detect_mF7E06C6A16E6005CBA62AC5BBD89CABD3032E12E (Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* __this, Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___picture0, Action_1_t267C05F8874B612902659095FC16875EF00A902D* ___callback1, const RuntimeMethod* method) ;
// UnityEngine.AsyncOperation UnityEngine.Resources::UnloadUnusedAssets()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C* Resources_UnloadUnusedAssets_m4003CD3EBC3AC2738DE9F2960D5BC45818C1F12B (const RuntimeMethod* method) ;
// UnityEngine.Texture2D PhoneCamera::Scale(UnityEngine.Texture2D,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* PhoneCamera_Scale_m9A8BA55FFE200580A45E79CAD587A84A5CA54908 (PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* __this, Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___texture0, int32_t ___imageSize1, const RuntimeMethod* method) ;
// UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* Texture2D_GetPixels32_m48230192E7543765C1A517F251D1D9BFCFB58C3D (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, const RuntimeMethod* method) ;
// UnityEngine.Color32[] PhoneCamera::Rotate(UnityEngine.Color32[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* PhoneCamera_Rotate_m52B13D25E4B4F2222D4B09FDF6E68D4F38CDB84A (PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* __this, Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___pixels0, int32_t ___width1, int32_t ___height2, const RuntimeMethod* method) ;
// System.Void System.Action`1<UnityEngine.Color32[]>::Invoke(T)
inline void Action_1_Invoke_mA379E47BAF47659F172826042396C270CD777D00_inline (Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9* __this, Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9*, Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259*, const RuntimeMethod*))Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline)(__this, ___obj0, method);
}
// System.Void PhoneCamera/<>c__DisplayClass14_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_0__ctor_mBCB0D3117D67C026EBB00245AFD9AB9A7E8962C8 (U3CU3Ec__DisplayClass14_0_tFF5B9D2B76EB8F350A918BA71C6925E798589AA8* __this, const RuntimeMethod* method) ;
// System.Void System.Action`1<UnityEngine.Texture2D>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m0D74126D5BBFFF417FDA1881B8AEBD833A833F76 (Action_1_tD7F74291FAA5A362F4A5F48D4E1CF3ED51FA6A33* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tD7F74291FAA5A362F4A5F48D4E1CF3ED51FA6A33*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_1__ctor_m2E1DFA67718FC1A0B6E5DFEB78831FFE9C059EB4_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.IEnumerator TFClassify.TextureTools::CropSquare(UnityEngine.WebCamTexture,TFClassify.TextureTools/RectOptions,System.Action`1<UnityEngine.Texture2D>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TextureTools_CropSquare_m8CFFF250CD0D934710F6D15DE737D91AAE29B96E (WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* ___texture0, int32_t ___rectOptions1, Action_1_tD7F74291FAA5A362F4A5F48D4E1CF3ED51FA6A33* ___callback2, const RuntimeMethod* method) ;
// System.Void TFClassify.TextureTools/<CropSquare>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCropSquareU3Ed__2__ctor_mFD8FDCD0E558E01DE3C96314D12284483696A1B3 (U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void TFClassify.TextureTools::_gpu_scale(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.FilterMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextureTools__gpu_scale_m41EB6FEFF1926369E00E6254F6F07C2B8BEDE8DF (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___src0, int32_t ___width1, int32_t ___height2, int32_t ___fmode3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_mECF60A9EC0638EC353C02C8E99B6B465D23BE917 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, int32_t ___width0, int32_t ___height1, int32_t ___textureFormat2, bool ___mipChain3, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Texture2D::Reinitialize(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Texture2D_Reinitialize_m9AB4169DA359C18BB4102F8E00C4321B53714E6B (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_ReadPixels_m7483DB211233F02E46418E9A6077487925F0024C (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___source0, int32_t ___destX1, int32_t ___destY2, bool ___recalculateMipMaps3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture_set_filterMode_mE423E58C0C16D059EA62BA87AD70F44AEA50CCC9 (Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* __this, int32_t ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::Apply(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_Apply_mCC369BCAB2D3AD3EE50EE01DA67AF227865FA2B3 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, bool ___updateMipmaps0, const RuntimeMethod* method) ;
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTexture__ctor_m45EACC89DDF408948889586516B3CA7AA8B73BFA (RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* __this, int32_t ___width0, int32_t ___height1, int32_t ___depth2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Graphics::SetRenderTarget(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graphics_SetRenderTarget_m995C0F14B97C5BF46CCF2E7EF410C1CC05C46409 (RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___rt0, const RuntimeMethod* method) ;
// System.Void UnityEngine.GL::LoadPixelMatrix(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GL_LoadPixelMatrix_mF1C5A4508C5F110512C116A5DDE7AB0483FE961A (float ___left0, float ___right1, float ___bottom2, float ___top3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) ;
// System.Void UnityEngine.GL::Clear(System.Boolean,System.Boolean,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GL_Clear_mA172E771FC32B516DB826F537832307C3A16BE09 (bool ___clearDepth0, bool ___clearColor1, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___backgroundColor2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graphics_DrawTexture_m400F92CB13445A7BC054BC074B7073EA7E4B322F (Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___screenRect0, Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___texture1, const RuntimeMethod* method) ;
// UnityEngine.Color32[] TFClassify.TextureTools::rotateSquare(UnityEngine.Color32[],System.Int32,System.Int32,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* TextureTools_rotateSquare_m39D669E9FC88B16402F908880E5796BA7749E2DF (Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___arr0, int32_t ___width1, int32_t ___height2, double ___phi3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color32__ctor_mC9C6B443F0C7CA3F8B174158B2AF6F05E18EAC4E_inline (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method) ;
// System.Single UnityEngine.Rect::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_height_mE1AA6C6C725CCD2D317BD2157396D3CF7D47C9D8 (Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Rect::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_width_m620D67551372073C9C32C4C4624C2A5713F7F9A9 (Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D* __this, const RuntimeMethod* method) ;
// System.Void System.ArgumentException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465 (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* __this, String_t* ___message0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_m3BA82E87442B7F69E118477069AE11101B9DF796 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method) ;
// System.Single UnityEngine.Rect::get_x()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_x_mB267B718E0D067F2BAE31BA477647FBF964916EB (Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Rect::get_y()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_y_mC733E8D49F3CE21B2A3D40A1B72D687F22C97F49 (Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_FloorToInt_m2A39AE881CAEE6B6A4B3BFEF9CA1ED40625F5AB7_inline (float ___f0, const RuntimeMethod* method) ;
// UnityEngine.Color[] UnityEngine.WebCamTexture::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* WebCamTexture_GetPixels_mD30134473515AEA70C9DE43392F2ADD95747237A (WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_SetPixels_mAE0CDFA15FA96F840D7FFADC31405D8AF20D9073 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* ___colors0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::Apply()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_Apply_mA014182C9EE0BBF6EEE3B286854F29E50EB972DC (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, const RuntimeMethod* method) ;
// System.Void System.Action`1<UnityEngine.Texture2D>::Invoke(T)
inline void Action_1_Invoke_mCBAA13E9863B2958F53D13FBB8B1B51CA0011B53_inline (Action_1_tD7F74291FAA5A362F4A5F48D4E1CF3ED51FA6A33* __this, Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tD7F74291FAA5A362F4A5F48D4E1CF3ED51FA6A33*, Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4*, const RuntimeMethod*))Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline)(__this, ___obj0, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Detector::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Detector_Start_mD69279C5B5CE102BCB490E346CB0E4679C148C43 (Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* __this, const RuntimeMethod* method) 
{
	Model_t8F5B69764BF210D112B40801428554C916789420* V_0 = NULL;
	{
		// var model = ModelLoader.Load(this.modelFile);
		NNModel_tE7CC55654A5FE992DAE3714CDB0A4711FB9A7153* L_0 = __this->___modelFile_4;
		Model_t8F5B69764BF210D112B40801428554C916789420* L_1;
		L_1 = ModelLoader_Load_mDADBCE3F4F7F98CECFD6531B7C752ABAFBD8F0C0(L_0, (bool)0, (bool)0, NULL);
		V_0 = L_1;
		// this.worker = WorkerFactory.CreateWorker(model, WorkerFactory.Device.GPU);
		Model_t8F5B69764BF210D112B40801428554C916789420* L_2 = V_0;
		RuntimeObject* L_3;
		L_3 = WorkerFactory_CreateWorker_mD9C277F1AC92D6532A85E84C5CABFD0C35509D88(L_2, ((int32_t)256), (bool)0, NULL);
		__this->___worker_12 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___worker_12), (void*)L_3);
		// }
		return;
	}
}
// System.Collections.IEnumerator Detector::Detect(UnityEngine.Color32[],System.Action`1<System.Collections.Generic.IList`1<BoundingBox>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Detector_Detect_mF7E06C6A16E6005CBA62AC5BBD89CABD3032E12E (Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* __this, Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___picture0, Action_1_t267C05F8874B612902659095FC16875EF00A902D* ___callback1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4* L_0 = (U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4*)il2cpp_codegen_object_new(U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CDetectU3Ed__18__ctor_mDC24DE8F8D42EAEBF097B384F1D425CBFEDC01DA(L_0, 0, NULL);
		U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_3 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_3), (void*)__this);
		U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4* L_2 = L_1;
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_3 = ___picture0;
		NullCheck(L_2);
		L_2->___picture_2 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&L_2->___picture_2), (void*)L_3);
		U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4* L_4 = L_2;
		Action_1_t267C05F8874B612902659095FC16875EF00A902D* L_5 = ___callback1;
		NullCheck(L_4);
		L_4->___callback_4 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&L_4->___callback_4), (void*)L_5);
		return L_4;
	}
}
// Unity.Barracuda.Tensor Detector::TransformInput(UnityEngine.Color32[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* Detector_TransformInput_m70DB7B82E6F85F933AEBB34D98CCD62F67C12116 (Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___pic0, int32_t ___width1, int32_t ___height2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* V_0 = NULL;
	int32_t V_1 = 0;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// float[] floatValues = new float[width * height * 3];
		int32_t L_0 = ___width1;
		int32_t L_1 = ___height2;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_2 = (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)SZArrayNew(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_multiply(((int32_t)il2cpp_codegen_multiply(L_0, L_1)), 3)));
		V_0 = L_2;
		// for (int i = 0; i < pic.Length; ++i)
		V_1 = 0;
		goto IL_0055;
	}

IL_000f:
	{
		// var color = pic[i];
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_3 = ___pic0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_2 = L_6;
		// floatValues[i * 3 + 0] = (color.r - IMAGE_MEAN) / IMAGE_STD;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_7 = V_0;
		int32_t L_8 = V_1;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_9 = V_2;
		uint8_t L_10 = L_9.___r_1;
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_multiply(L_8, 3))), (float)((float)(((float)L_10)/(1.0f))));
		// floatValues[i * 3 + 1] = (color.g - IMAGE_MEAN) / IMAGE_STD;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_11 = V_0;
		int32_t L_12 = V_1;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_13 = V_2;
		uint8_t L_14 = L_13.___g_2;
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_12, 3)), 1))), (float)((float)(((float)L_14)/(1.0f))));
		// floatValues[i * 3 + 2] = (color.b - IMAGE_MEAN) / IMAGE_STD;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_15 = V_0;
		int32_t L_16 = V_1;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_17 = V_2;
		uint8_t L_18 = L_17.___b_3;
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_16, 3)), 2))), (float)((float)(((float)L_18)/(1.0f))));
		// for (int i = 0; i < pic.Length; ++i)
		int32_t L_19 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_0055:
	{
		// for (int i = 0; i < pic.Length; ++i)
		int32_t L_20 = V_1;
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_21 = ___pic0;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)((int32_t)(((RuntimeArray*)L_21)->max_length)))))
		{
			goto IL_000f;
		}
	}
	{
		// return new Tensor(1, height, width, 3, floatValues);
		int32_t L_22 = ___height2;
		int32_t L_23 = ___width1;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_24 = V_0;
		Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* L_25 = (Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10*)il2cpp_codegen_object_new(Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10_il2cpp_TypeInfo_var);
		NullCheck(L_25);
		Tensor__ctor_m6F4A668FB0CD6ACD79447138D103F9445894BDDA(L_25, 1, L_22, L_23, 3, L_24, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
		return L_25;
	}
}
// System.Collections.Generic.IList`1<BoundingBox> Detector::ParseOutputs(Unity.Barracuda.Tensor,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Detector_ParseOutputs_m43D3C60ECD5961E7398A77E019855B701EA97D05 (Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* __this, Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* ___yoloModelOutput0, float ___threshold1, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* V_5 = NULL;
	int32_t V_6 = 0;
	{
		// for (int cy = 0; cy < COL_COUNT; cy++)
		V_0 = 0;
		goto IL_0072;
	}

IL_0004:
	{
		// for (int cx = 0; cx < ROW_COUNT; cx++)
		V_1 = 0;
		goto IL_0069;
	}

IL_0008:
	{
		// for (int box = 0; box < BOXES_PER_CELL; box++)
		V_2 = 0;
		goto IL_0061;
	}

IL_000c:
	{
		// var channel = (box * (CLASS_COUNT + BOX_INFO_FEATURE_COUNT));
		int32_t L_0 = V_2;
		V_3 = ((int32_t)il2cpp_codegen_multiply(L_0, ((int32_t)25)));
		// float confidence = GetConfidence(yoloModelOutput, cx, cy, channel);
		Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* L_1 = ___yoloModelOutput0;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		int32_t L_4 = V_3;
		float L_5;
		L_5 = Detector_GetConfidence_mD04080750424C52D2710A6385C9A80FAC64C54AE(__this, L_1, L_2, L_3, L_4, NULL);
		V_4 = L_5;
		// if (confidence < threshold)
		float L_6 = V_4;
		float L_7 = ___threshold1;
		if ((((float)L_6) < ((float)L_7)))
		{
			goto IL_005d;
		}
	}
	{
		// float[] predictedClasses = ExtractClasses(yoloModelOutput, cx, cy, channel);
		Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* L_8 = ___yoloModelOutput0;
		int32_t L_9 = V_1;
		int32_t L_10 = V_0;
		int32_t L_11 = V_3;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_12;
		L_12 = Detector_ExtractClasses_mBE2BC473855937D06B745973F3745B8A51299806(__this, L_8, L_9, L_10, L_11, NULL);
		V_5 = L_12;
		// var (topResultIndex, topResultScore) = GetTopResult(predictedClasses);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_13 = V_5;
		ValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69 L_14;
		L_14 = Detector_GetTopResult_m77F06CCB4F2570BD7D8F9A8E5877108CEA0B610B(__this, L_13, NULL);
		ValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69 L_15 = L_14;
		int32_t L_16 = L_15.___Item1_0;
		V_6 = L_16;
		float L_17 = L_15.___Item2_1;
		// var topScore = topResultScore * confidence;
		float L_18 = V_4;
		// if (topScore < threshold)
		float L_19 = ___threshold1;
		if ((((float)((float)il2cpp_codegen_multiply(L_17, L_18))) < ((float)L_19)))
		{
			goto IL_005d;
		}
	}
	{
		// _text.text = labels[topResultIndex];
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_20 = __this->____text_5;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_21 = __this->___labels_20;
		int32_t L_22 = V_6;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		String_t* L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_20);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_20, L_24);
	}

IL_005d:
	{
		// for (int box = 0; box < BOXES_PER_CELL; box++)
		int32_t L_25 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_25, 1));
	}

IL_0061:
	{
		// for (int box = 0; box < BOXES_PER_CELL; box++)
		int32_t L_26 = V_2;
		if ((((int32_t)L_26) < ((int32_t)5)))
		{
			goto IL_000c;
		}
	}
	{
		// for (int cx = 0; cx < ROW_COUNT; cx++)
		int32_t L_27 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_27, 1));
	}

IL_0069:
	{
		// for (int cx = 0; cx < ROW_COUNT; cx++)
		int32_t L_28 = V_1;
		if ((((int32_t)L_28) < ((int32_t)((int32_t)13))))
		{
			goto IL_0008;
		}
	}
	{
		// for (int cy = 0; cy < COL_COUNT; cy++)
		int32_t L_29 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_29, 1));
	}

IL_0072:
	{
		// for (int cy = 0; cy < COL_COUNT; cy++)
		int32_t L_30 = V_0;
		if ((((int32_t)L_30) < ((int32_t)((int32_t)13))))
		{
			goto IL_0004;
		}
	}
	{
		// return null;
		return (RuntimeObject*)NULL;
	}
}
// System.Single Detector::Sigmoid(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Detector_Sigmoid_mCF05C6BD26BEC3884F205912F1BD56865E3B28D0 (Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* __this, float ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// var k = (float)Math.Exp(value);
		float L_0 = ___value0;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = exp(((double)L_0));
		V_0 = ((float)L_1);
		// return k / (1.0f + k);
		float L_2 = V_0;
		float L_3 = V_0;
		return ((float)(L_2/((float)il2cpp_codegen_add((1.0f), L_3))));
	}
}
// System.Single[] Detector::Softmax(System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* Detector_Softmax_mC92F5060627D870E16B67ABE6FE364F25D2C16CA (Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* __this, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___values0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Select_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m963F5737975A51BD55277B738111AA7E3EDD66AF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Select_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_mBFE23CA2A88CF76FCB150F230F156D046097B9CB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mF0AED1993946FF775115C47514B29636472220A1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t30CC93434BF7EFA66AF4592260CCDBC8BFC5DD87_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t4C40E8255CD677D8102C7DF0D02FF06A36B97E3F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass22_0_U3CSoftmaxU3Eb__0_m77CBFD936F493D01E896B0E518BC436EE3B3845E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass22_0_U3CSoftmaxU3Eb__1_mE9F6EA14222CF19AF3BDA5217A977C5BAEAFE5DF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass22_0_t11D348A067DFC3081F35C9A414BC9626CDD04866_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass22_0_t11D348A067DFC3081F35C9A414BC9626CDD04866* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3CU3Ec__DisplayClass22_0_t11D348A067DFC3081F35C9A414BC9626CDD04866* L_0 = (U3CU3Ec__DisplayClass22_0_t11D348A067DFC3081F35C9A414BC9626CDD04866*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass22_0_t11D348A067DFC3081F35C9A414BC9626CDD04866_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__DisplayClass22_0__ctor_m31724DA5864B4C47CF318E8E1956EAF7D971E383(L_0, NULL);
		V_0 = L_0;
		// var maxVal = values.Max();
		U3CU3Ec__DisplayClass22_0_t11D348A067DFC3081F35C9A414BC9626CDD04866* L_1 = V_0;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_2 = ___values0;
		float L_3;
		L_3 = Enumerable_Max_m3215FA2A65ACAFE226A0FBE065D772C7CE1A5E9F((RuntimeObject*)L_2, NULL);
		NullCheck(L_1);
		L_1->___maxVal_0 = L_3;
		// var exp = values.Select(v => Math.Exp(v - maxVal));
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_4 = ___values0;
		U3CU3Ec__DisplayClass22_0_t11D348A067DFC3081F35C9A414BC9626CDD04866* L_5 = V_0;
		Func_2_t30CC93434BF7EFA66AF4592260CCDBC8BFC5DD87* L_6 = (Func_2_t30CC93434BF7EFA66AF4592260CCDBC8BFC5DD87*)il2cpp_codegen_object_new(Func_2_t30CC93434BF7EFA66AF4592260CCDBC8BFC5DD87_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		Func_2__ctor_m5A0602CB85D5CD42F7B0529A5C0435342FD0001F(L_6, L_5, (intptr_t)((void*)U3CU3Ec__DisplayClass22_0_U3CSoftmaxU3Eb__0_m77CBFD936F493D01E896B0E518BC436EE3B3845E_RuntimeMethod_var), NULL);
		RuntimeObject* L_7;
		L_7 = Enumerable_Select_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_mBFE23CA2A88CF76FCB150F230F156D046097B9CB((RuntimeObject*)L_4, L_6, Enumerable_Select_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_mBFE23CA2A88CF76FCB150F230F156D046097B9CB_RuntimeMethod_var);
		V_1 = L_7;
		// var sumExp = exp.Sum();
		U3CU3Ec__DisplayClass22_0_t11D348A067DFC3081F35C9A414BC9626CDD04866* L_8 = V_0;
		RuntimeObject* L_9 = V_1;
		double L_10;
		L_10 = Enumerable_Sum_mB4B29B0D6E567EB810D0B439945F9BC6ACC01284(L_9, NULL);
		NullCheck(L_8);
		L_8->___sumExp_1 = L_10;
		// return exp.Select(v => (float)(v / sumExp)).ToArray();
		RuntimeObject* L_11 = V_1;
		U3CU3Ec__DisplayClass22_0_t11D348A067DFC3081F35C9A414BC9626CDD04866* L_12 = V_0;
		Func_2_t4C40E8255CD677D8102C7DF0D02FF06A36B97E3F* L_13 = (Func_2_t4C40E8255CD677D8102C7DF0D02FF06A36B97E3F*)il2cpp_codegen_object_new(Func_2_t4C40E8255CD677D8102C7DF0D02FF06A36B97E3F_il2cpp_TypeInfo_var);
		NullCheck(L_13);
		Func_2__ctor_m8B3A2701A41C48403E8F3F0B902AD81B31F18A67(L_13, L_12, (intptr_t)((void*)U3CU3Ec__DisplayClass22_0_U3CSoftmaxU3Eb__1_mE9F6EA14222CF19AF3BDA5217A977C5BAEAFE5DF_RuntimeMethod_var), NULL);
		RuntimeObject* L_14;
		L_14 = Enumerable_Select_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m963F5737975A51BD55277B738111AA7E3EDD66AF(L_11, L_13, Enumerable_Select_TisDouble_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_m963F5737975A51BD55277B738111AA7E3EDD66AF_RuntimeMethod_var);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_15;
		L_15 = Enumerable_ToArray_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mF0AED1993946FF775115C47514B29636472220A1(L_14, Enumerable_ToArray_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mF0AED1993946FF775115C47514B29636472220A1_RuntimeMethod_var);
		return L_15;
	}
}
// System.Single Detector::GetConfidence(Unity.Barracuda.Tensor,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Detector_GetConfidence_mD04080750424C52D2710A6385C9A80FAC64C54AE (Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* __this, Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* ___modelOutput0, int32_t ___x1, int32_t ___y2, int32_t ___channel3, const RuntimeMethod* method) 
{
	{
		// return Sigmoid(modelOutput[0, x, y, channel + 4]);
		Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* L_0 = ___modelOutput0;
		int32_t L_1 = ___x1;
		int32_t L_2 = ___y2;
		int32_t L_3 = ___channel3;
		NullCheck(L_0);
		float L_4;
		L_4 = Tensor_get_Item_mA53C387CBDB9F0EA0D069DD7BFC45970172290C5(L_0, 0, L_1, L_2, ((int32_t)il2cpp_codegen_add(L_3, 4)), NULL);
		float L_5;
		L_5 = Detector_Sigmoid_mCF05C6BD26BEC3884F205912F1BD56865E3B28D0(__this, L_4, NULL);
		return L_5;
	}
}
// System.Single[] Detector::ExtractClasses(Unity.Barracuda.Tensor,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* Detector_ExtractClasses_mBE2BC473855937D06B745973F3745B8A51299806 (Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* __this, Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* ___modelOutput0, int32_t ___x1, int32_t ___y2, int32_t ___channel3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// float[] predictedClasses = new float[CLASS_COUNT];
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_0 = (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)SZArrayNew(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var, (uint32_t)((int32_t)20));
		V_0 = L_0;
		// int predictedClassOffset = channel + BOX_INFO_FEATURE_COUNT;
		int32_t L_1 = ___channel3;
		V_1 = ((int32_t)il2cpp_codegen_add(L_1, 5));
		// for (int predictedClass = 0; predictedClass < CLASS_COUNT; predictedClass++)
		V_2 = 0;
		goto IL_0024;
	}

IL_0011:
	{
		// predictedClasses[predictedClass] = modelOutput[0, x, y, predictedClass + predictedClassOffset];
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_2 = V_0;
		int32_t L_3 = V_2;
		Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* L_4 = ___modelOutput0;
		int32_t L_5 = ___x1;
		int32_t L_6 = ___y2;
		int32_t L_7 = V_2;
		int32_t L_8 = V_1;
		NullCheck(L_4);
		float L_9;
		L_9 = Tensor_get_Item_mA53C387CBDB9F0EA0D069DD7BFC45970172290C5(L_4, 0, L_5, L_6, ((int32_t)il2cpp_codegen_add(L_7, L_8)), NULL);
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (float)L_9);
		// for (int predictedClass = 0; predictedClass < CLASS_COUNT; predictedClass++)
		int32_t L_10 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_10, 1));
	}

IL_0024:
	{
		// for (int predictedClass = 0; predictedClass < CLASS_COUNT; predictedClass++)
		int32_t L_11 = V_2;
		if ((((int32_t)L_11) < ((int32_t)((int32_t)20))))
		{
			goto IL_0011;
		}
	}
	{
		// return Softmax(predictedClasses);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_12 = V_0;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_13;
		L_13 = Detector_Softmax_mC92F5060627D870E16B67ABE6FE364F25D2C16CA(__this, L_12, NULL);
		return L_13;
	}
}
// System.ValueTuple`2<System.Int32,System.Single> Detector::GetTopResult(System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69 Detector_GetTopResult_m77F06CCB4F2570BD7D8F9A8E5877108CEA0B610B (Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* __this, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___predictedClasses0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_First_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_m8CC5D68086B010D8F0DE7BC315C75B2F2244EEC5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_OrderByDescending_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mE32646C7E23945034DDC893CFDD067B739F2188A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Select_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_m723EFB342860B437724ECE366144EB550DEC1E73_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CGetTopResultU3Eb__25_0_m6BF281AB15D6FA7FEAF74F680602ED2A2EA4781D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CGetTopResultU3Eb__25_1_m8954BE50605939235DCDA5D794EE0A7245021E74_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266* G_B2_0 = NULL;
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* G_B2_1 = NULL;
	Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266* G_B1_0 = NULL;
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* G_B1_1 = NULL;
	Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63* G_B4_0 = NULL;
	RuntimeObject* G_B4_1 = NULL;
	Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63* G_B3_0 = NULL;
	RuntimeObject* G_B3_1 = NULL;
	{
		// return predictedClasses
		//     .Select((predictedClass, index) => (Index: index, Value: predictedClass))
		//     .OrderByDescending(result => result.Value)
		//     .First();
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_0 = ___predictedClasses0;
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var);
		Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266* L_1 = ((U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var))->___U3CU3E9__25_0_1;
		Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266* L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			goto IL_0020;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var);
		U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3* L_3 = ((U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var))->___U3CU3E9_0;
		Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266* L_4 = (Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266*)il2cpp_codegen_object_new(Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		Func_3__ctor_m66045AE411F68F153ADA7C2D06A34602ADB1E316(L_4, L_3, (intptr_t)((void*)U3CU3Ec_U3CGetTopResultU3Eb__25_0_m6BF281AB15D6FA7FEAF74F680602ED2A2EA4781D_RuntimeMethod_var), NULL);
		Func_3_t3AC4212F5290D97B2AE376A8249661F8AC501266* L_5 = L_4;
		((U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var))->___U3CU3E9__25_0_1 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var))->___U3CU3E9__25_0_1), (void*)L_5);
		G_B2_0 = L_5;
		G_B2_1 = G_B1_1;
	}

IL_0020:
	{
		RuntimeObject* L_6;
		L_6 = Enumerable_Select_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_m723EFB342860B437724ECE366144EB550DEC1E73((RuntimeObject*)G_B2_1, G_B2_0, Enumerable_Select_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_m723EFB342860B437724ECE366144EB550DEC1E73_RuntimeMethod_var);
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var);
		Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63* L_7 = ((U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var))->___U3CU3E9__25_1_2;
		Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63* L_8 = L_7;
		G_B3_0 = L_8;
		G_B3_1 = L_6;
		if (L_8)
		{
			G_B4_0 = L_8;
			G_B4_1 = L_6;
			goto IL_0044;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var);
		U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3* L_9 = ((U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var))->___U3CU3E9_0;
		Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63* L_10 = (Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63*)il2cpp_codegen_object_new(Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63_il2cpp_TypeInfo_var);
		NullCheck(L_10);
		Func_2__ctor_mAC4A9C9324857AE713A728A56F2729F6430F48A2(L_10, L_9, (intptr_t)((void*)U3CU3Ec_U3CGetTopResultU3Eb__25_1_m8954BE50605939235DCDA5D794EE0A7245021E74_RuntimeMethod_var), NULL);
		Func_2_t53AE14A95662BD3BFC7B4EC8DD8CC8EB8311DA63* L_11 = L_10;
		((U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var))->___U3CU3E9__25_1_2 = L_11;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var))->___U3CU3E9__25_1_2), (void*)L_11);
		G_B4_0 = L_11;
		G_B4_1 = G_B3_1;
	}

IL_0044:
	{
		RuntimeObject* L_12;
		L_12 = Enumerable_OrderByDescending_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mE32646C7E23945034DDC893CFDD067B739F2188A(G_B4_1, G_B4_0, Enumerable_OrderByDescending_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_TisSingle_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_mE32646C7E23945034DDC893CFDD067B739F2188A_RuntimeMethod_var);
		ValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69 L_13;
		L_13 = Enumerable_First_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_m8CC5D68086B010D8F0DE7BC315C75B2F2244EEC5(L_12, Enumerable_First_TisValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69_m8CC5D68086B010D8F0DE7BC315C75B2F2244EEC5_RuntimeMethod_var);
		return L_13;
	}
}
// System.Void Detector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Detector__ctor_mAD82AD1BAB8A450465B81A86CAB292C8A3EBCB3E (Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0FF1A933F6BC927FBAD982850B394C6D44B3F3F2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral141DFBA76E0AF13B947128EAEE3242F482BBE356);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral142E7F04AD15522E43403C64A524E017226383F5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral18345A1BE3A027B3306C286DC6C40328CFFF3DAE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1851E5597ABA51CECE59A7F352CB76C7D75DAF9D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1FC4D8FB742A77362133C567770CFD4F762AAEC7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral29CEE6C774C2A6B13349634187AB40A780AAC0C0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3614C1A34A3BD65AE2BB7EF403804AA15E305D3F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3E9672DA3C80A70E01B048EBA83E47A9CC57CB08);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral58996B06CA854E96768D85CEA7C32D4965B0C68C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5AD43F7D251766D7095E126E2046470EEF1FE594);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral78622C01FE15EA71654ECF91AA6E911E2B4BECA1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9053C7BE1B5533FB96F3DE70C35EA0FFAB4FE19E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral93B98C3040BF26B2D68E7DD514A4EE37837430F3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB994FC3AD57A7150DAF257DF07AB5DB6E6BEFE04);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBC9A54DB61539D95B06AD135584A19F1C3A21E80);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC988FC027F8EF913F6D5AF5335C7D6DEAF47584B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDCBB48108EF25E2866BD80E6F7E7A1859DAFC8EB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDE7C2CB5991EEEC83B8129FF035C6C8425992CC6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEC47AE1BED75BF8A740DADA47F96B6270D14637C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private string[] labels = {
		//                             "aeroplane","bicycle","bird","boat","bottle",
		//                             "bus","car","cat","chair","cow",
		//                             "diningtable","dog","horse","motorbike","person",
		//                             "pottedplant","sheep","sofa","train","tvmonitor"
		//                           };
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_0 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)((int32_t)20));
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral3E9672DA3C80A70E01B048EBA83E47A9CC57CB08);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3E9672DA3C80A70E01B048EBA83E47A9CC57CB08);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral1851E5597ABA51CECE59A7F352CB76C7D75DAF9D);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral1851E5597ABA51CECE59A7F352CB76C7D75DAF9D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral1FC4D8FB742A77362133C567770CFD4F762AAEC7);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1FC4D8FB742A77362133C567770CFD4F762AAEC7);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral18345A1BE3A027B3306C286DC6C40328CFFF3DAE);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral18345A1BE3A027B3306C286DC6C40328CFFF3DAE);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = L_4;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral5AD43F7D251766D7095E126E2046470EEF1FE594);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral5AD43F7D251766D7095E126E2046470EEF1FE594);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral9053C7BE1B5533FB96F3DE70C35EA0FFAB4FE19E);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral9053C7BE1B5533FB96F3DE70C35EA0FFAB4FE19E);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral78622C01FE15EA71654ECF91AA6E911E2B4BECA1);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral78622C01FE15EA71654ECF91AA6E911E2B4BECA1);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_8 = L_7;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral93B98C3040BF26B2D68E7DD514A4EE37837430F3);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral93B98C3040BF26B2D68E7DD514A4EE37837430F3);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_9 = L_8;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral142E7F04AD15522E43403C64A524E017226383F5);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral142E7F04AD15522E43403C64A524E017226383F5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_10 = L_9;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteralB994FC3AD57A7150DAF257DF07AB5DB6E6BEFE04);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteralB994FC3AD57A7150DAF257DF07AB5DB6E6BEFE04);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteralC988FC027F8EF913F6D5AF5335C7D6DEAF47584B);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteralC988FC027F8EF913F6D5AF5335C7D6DEAF47584B);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral58996B06CA854E96768D85CEA7C32D4965B0C68C);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteral58996B06CA854E96768D85CEA7C32D4965B0C68C);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral29CEE6C774C2A6B13349634187AB40A780AAC0C0);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (String_t*)_stringLiteral29CEE6C774C2A6B13349634187AB40A780AAC0C0);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = L_13;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral0FF1A933F6BC927FBAD982850B394C6D44B3F3F2);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (String_t*)_stringLiteral0FF1A933F6BC927FBAD982850B394C6D44B3F3F2);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = L_14;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteralBC9A54DB61539D95B06AD135584A19F1C3A21E80);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (String_t*)_stringLiteralBC9A54DB61539D95B06AD135584A19F1C3A21E80);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_16 = L_15;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteralDE7C2CB5991EEEC83B8129FF035C6C8425992CC6);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (String_t*)_stringLiteralDE7C2CB5991EEEC83B8129FF035C6C8425992CC6);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_17 = L_16;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral3614C1A34A3BD65AE2BB7EF403804AA15E305D3F);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (String_t*)_stringLiteral3614C1A34A3BD65AE2BB7EF403804AA15E305D3F);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral141DFBA76E0AF13B947128EAEE3242F482BBE356);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (String_t*)_stringLiteral141DFBA76E0AF13B947128EAEE3242F482BBE356);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = L_18;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteralEC47AE1BED75BF8A740DADA47F96B6270D14637C);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (String_t*)_stringLiteralEC47AE1BED75BF8A740DADA47F96B6270D14637C);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = L_19;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteralDCBB48108EF25E2866BD80E6F7E7A1859DAFC8EB);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (String_t*)_stringLiteralDCBB48108EF25E2866BD80E6F7E7A1859DAFC8EB);
		__this->___labels_20 = L_20;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___labels_20), (void*)L_20);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Detector/<Detect>d__18::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDetectU3Ed__18__ctor_mDC24DE8F8D42EAEBF097B384F1D425CBFEDC01DA (U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void Detector/<Detect>d__18::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDetectU3Ed__18_System_IDisposable_Dispose_m612F41816C95EE746B09DFF12C97156E88C27218 (U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_001a;
		}
	}

IL_0010:
	{
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0013:
			{// begin finally (depth: 1)
				U3CDetectU3Ed__18_U3CU3Em__Finally1_m21985138C6A5A3A76754E0480D83BA0AA29FAA94(__this, NULL);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			goto IL_001a;
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean Detector/<Detect>d__18::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CDetectU3Ed__18_MoveNext_mD6488F35519BAD5A786DEE3361502045F1FEACFD (U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IWorker_t4B51670416E1CA4C2A1E15794C2ED9905D464555_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9A7F9DE0C8F3D56FCC7CD55AC4B41D661174ABE2);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* V_2 = NULL;
	Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* V_3 = NULL;
	{
		auto __finallyBlock = il2cpp::utils::Fault([&]
		{

FAULT_00b1:
			{// begin fault (depth: 1)
				U3CDetectU3Ed__18_System_IDisposable_Dispose_m612F41816C95EE746B09DFF12C97156E88C27218(__this, NULL);
				return;
			}// end fault
		});
		try
		{// begin try (depth: 1)
			{
				int32_t L_0 = __this->___U3CU3E1__state_0;
				V_1 = L_0;
				Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* L_1 = __this->___U3CU3E4__this_3;
				V_2 = L_1;
				int32_t L_2 = V_1;
				if (!L_2)
				{
					goto IL_001c_1;
				}
			}
			{
				int32_t L_3 = V_1;
				if ((((int32_t)L_3) == ((int32_t)1)))
				{
					goto IL_006e_1;
				}
			}
			{
				V_0 = (bool)0;
				goto IL_00b8;
			}

IL_001c_1:
			{
				__this->___U3CU3E1__state_0 = (-1);
				// using (var tensor = TransformInput(picture, IMAGE_SIZE, IMAGE_SIZE))
				Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_4 = __this->___picture_2;
				Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* L_5;
				L_5 = Detector_TransformInput_m70DB7B82E6F85F933AEBB34D98CCD62F67C12116(L_4, ((int32_t)416), ((int32_t)416), NULL);
				__this->___U3CtensorU3E5__2_5 = L_5;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CtensorU3E5__2_5), (void*)L_5);
				__this->___U3CU3E1__state_0 = ((int32_t)-3);
				// yield return StartCoroutine(worker.StartManualSchedule(tensor));//inputs
				Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* L_6 = V_2;
				Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* L_7 = V_2;
				NullCheck(L_7);
				RuntimeObject* L_8 = L_7->___worker_12;
				Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* L_9 = __this->___U3CtensorU3E5__2_5;
				NullCheck(L_8);
				RuntimeObject* L_10;
				L_10 = InterfaceFuncInvoker1< RuntimeObject*, Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* >::Invoke(7 /* System.Collections.IEnumerator Unity.Barracuda.IWorker::StartManualSchedule(Unity.Barracuda.Tensor) */, IWorker_t4B51670416E1CA4C2A1E15794C2ED9905D464555_il2cpp_TypeInfo_var, L_8, L_9);
				NullCheck(L_6);
				Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_11;
				L_11 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(L_6, L_10, NULL);
				__this->___U3CU3E2__current_1 = L_11;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_11);
				__this->___U3CU3E1__state_0 = 1;
				V_0 = (bool)1;
				goto IL_00b8;
			}

IL_006e_1:
			{
				__this->___U3CU3E1__state_0 = ((int32_t)-3);
				// var output = worker.PeekOutput(OUTPUT_NAME);
				Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* L_12 = V_2;
				NullCheck(L_12);
				RuntimeObject* L_13 = L_12->___worker_12;
				NullCheck(L_13);
				Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* L_14;
				L_14 = InterfaceFuncInvoker1< Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10*, String_t* >::Invoke(12 /* Unity.Barracuda.Tensor Unity.Barracuda.IWorker::PeekOutput(System.String) */, IWorker_t4B51670416E1CA4C2A1E15794C2ED9905D464555_il2cpp_TypeInfo_var, L_13, _stringLiteral9A7F9DE0C8F3D56FCC7CD55AC4B41D661174ABE2);
				V_3 = L_14;
				// var results = ParseOutputs(output, MINIMUM_CONFIDENCE);
				Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* L_15 = V_2;
				Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* L_16 = V_3;
				NullCheck(L_15);
				RuntimeObject* L_17;
				L_17 = Detector_ParseOutputs_m43D3C60ECD5961E7398A77E019855B701EA97D05(L_15, L_16, (0.300000012f), NULL);
				// callback(null);
				Action_1_t267C05F8874B612902659095FC16875EF00A902D* L_18 = __this->___callback_4;
				NullCheck(L_18);
				Action_1_Invoke_mFC32950FF57D2118CA6414DEED7119D840B46E21_inline(L_18, (RuntimeObject*)NULL, NULL);
				// }
				U3CDetectU3Ed__18_U3CU3Em__Finally1_m21985138C6A5A3A76754E0480D83BA0AA29FAA94(__this, NULL);
				__this->___U3CtensorU3E5__2_5 = (Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10*)NULL;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CtensorU3E5__2_5), (void*)(Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10*)NULL);
				// }
				V_0 = (bool)0;
				goto IL_00b8;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00b8:
	{
		bool L_19 = V_0;
		return L_19;
	}
}
// System.Void Detector/<Detect>d__18::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDetectU3Ed__18_U3CU3Em__Finally1_m21985138C6A5A3A76754E0480D83BA0AA29FAA94 (U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->___U3CU3E1__state_0 = (-1);
		Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* L_0 = __this->___U3CtensorU3E5__2_5;
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		Tensor_t9B89EEE59F673A469027FF5E441797C2E247AF10* L_1 = __this->___U3CtensorU3E5__2_5;
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_1);
	}

IL_001a:
	{
		return;
	}
}
// System.Object Detector/<Detect>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CDetectU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52E50F5C28F3897B382E49C3A5DA81B2E6CFC2B0 (U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void Detector/<Detect>d__18::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDetectU3Ed__18_System_Collections_IEnumerator_Reset_m20841F6BEFEF1B50F595EE9CAC1373A72B6EAC0B (U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CDetectU3Ed__18_System_Collections_IEnumerator_Reset_m20841F6BEFEF1B50F595EE9CAC1373A72B6EAC0B_RuntimeMethod_var)));
	}
}
// System.Object Detector/<Detect>d__18::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CDetectU3Ed__18_System_Collections_IEnumerator_get_Current_m0888C367CFA15FA1C56768471593FEA6A7DF361D (U3CDetectU3Ed__18_t1882598646BAD6FF881EFE51E1C7C450E09DF5E4* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Detector/<>c__DisplayClass22_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass22_0__ctor_m31724DA5864B4C47CF318E8E1956EAF7D971E383 (U3CU3Ec__DisplayClass22_0_t11D348A067DFC3081F35C9A414BC9626CDD04866* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Double Detector/<>c__DisplayClass22_0::<Softmax>b__0(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double U3CU3Ec__DisplayClass22_0_U3CSoftmaxU3Eb__0_m77CBFD936F493D01E896B0E518BC436EE3B3845E (U3CU3Ec__DisplayClass22_0_t11D348A067DFC3081F35C9A414BC9626CDD04866* __this, float ___v0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// var exp = values.Select(v => Math.Exp(v - maxVal));
		float L_0 = ___v0;
		float L_1 = __this->___maxVal_0;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_2;
		L_2 = exp(((double)((float)il2cpp_codegen_subtract(L_0, L_1))));
		return L_2;
	}
}
// System.Single Detector/<>c__DisplayClass22_0::<Softmax>b__1(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CU3Ec__DisplayClass22_0_U3CSoftmaxU3Eb__1_mE9F6EA14222CF19AF3BDA5217A977C5BAEAFE5DF (U3CU3Ec__DisplayClass22_0_t11D348A067DFC3081F35C9A414BC9626CDD04866* __this, double ___v0, const RuntimeMethod* method) 
{
	{
		// return exp.Select(v => (float)(v / sumExp)).ToArray();
		double L_0 = ___v0;
		double L_1 = __this->___sumExp_1;
		return ((float)((double)(L_0/L_1)));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Detector/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mEF8FE2A305DEFF27220E5E055CF2690D7896C4F3 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3* L_0 = (U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3*)il2cpp_codegen_object_new(U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CU3Ec__ctor_m67170BF871ACE0B683BA53B3E1B6F6104C866FD8(L_0, NULL);
		((U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var))->___U3CU3E9_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3_il2cpp_TypeInfo_var))->___U3CU3E9_0), (void*)L_0);
		return;
	}
}
// System.Void Detector/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m67170BF871ACE0B683BA53B3E1B6F6104C866FD8 (U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.ValueTuple`2<System.Int32,System.Single> Detector/<>c::<GetTopResult>b__25_0(System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69 U3CU3Ec_U3CGetTopResultU3Eb__25_0_m6BF281AB15D6FA7FEAF74F680602ED2A2EA4781D (U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3* __this, float ___predictedClass0, int32_t ___index1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ValueTuple_2__ctor_m8AD4450A363A5626EE8665BFDFA6B773D05EE581_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// .Select((predictedClass, index) => (Index: index, Value: predictedClass))
		int32_t L_0 = ___index1;
		float L_1 = ___predictedClass0;
		ValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69 L_2;
		memset((&L_2), 0, sizeof(L_2));
		ValueTuple_2__ctor_m8AD4450A363A5626EE8665BFDFA6B773D05EE581((&L_2), L_0, L_1, /*hidden argument*/ValueTuple_2__ctor_m8AD4450A363A5626EE8665BFDFA6B773D05EE581_RuntimeMethod_var);
		return L_2;
	}
}
// System.Single Detector/<>c::<GetTopResult>b__25_1(System.ValueTuple`2<System.Int32,System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CU3Ec_U3CGetTopResultU3Eb__25_1_m8954BE50605939235DCDA5D794EE0A7245021E74 (U3CU3Ec_tA602B3177F5611809BEA67E2885C7828163BD5B3* __this, ValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69 ___result0, const RuntimeMethod* method) 
{
	{
		// .OrderByDescending(result => result.Value)
		ValueTuple_2_t2ADD54A4634C073CBAB66A75153EA556FF32FB69 L_0 = ___result0;
		float L_1 = L_0.___Item2_1;
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single DimensionsBase::get_X()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DimensionsBase_get_X_m2AA39E00503C51C587F473D1ADBEC9CCB5D54F41 (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, const RuntimeMethod* method) 
{
	{
		// public float X { get; set; }
		float L_0 = __this->___U3CXU3Ek__BackingField_0;
		return L_0;
	}
}
// System.Void DimensionsBase::set_X(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DimensionsBase_set_X_mD9B14D9F026F7773AC1821A15DEB1F1040C80D7A (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// public float X { get; set; }
		float L_0 = ___value0;
		__this->___U3CXU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.Single DimensionsBase::get_Y()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DimensionsBase_get_Y_m170190F8D1531F544A40AB55817C1EC7EFD8A864 (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, const RuntimeMethod* method) 
{
	{
		// public float Y { get; set; }
		float L_0 = __this->___U3CYU3Ek__BackingField_1;
		return L_0;
	}
}
// System.Void DimensionsBase::set_Y(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DimensionsBase_set_Y_m1A01B024CAB8953A5D9317F3AB2713D0F3102E8F (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// public float Y { get; set; }
		float L_0 = ___value0;
		__this->___U3CYU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Single DimensionsBase::get_Height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DimensionsBase_get_Height_m6632C66BBCA8F5D5ABC7E95739DD3713DBA7E9BE (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, const RuntimeMethod* method) 
{
	{
		// public float Height { get; set; }
		float L_0 = __this->___U3CHeightU3Ek__BackingField_2;
		return L_0;
	}
}
// System.Void DimensionsBase::set_Height(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DimensionsBase_set_Height_m31B2BDC1EEE04DDD2AF3AFD0AEFE49F3646CB28B (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// public float Height { get; set; }
		float L_0 = ___value0;
		__this->___U3CHeightU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.Single DimensionsBase::get_Width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DimensionsBase_get_Width_m6B20834F813A905E59F902C802FAB97D21286ECF (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, const RuntimeMethod* method) 
{
	{
		// public float Width { get; set; }
		float L_0 = __this->___U3CWidthU3Ek__BackingField_3;
		return L_0;
	}
}
// System.Void DimensionsBase::set_Width(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DimensionsBase_set_Width_m9DB1DEBCC8DBA0E0EC7D7AE2EE499AA6D7304EE4 (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// public float Width { get; set; }
		float L_0 = ___value0;
		__this->___U3CWidthU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Void DimensionsBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DimensionsBase__ctor_m9AFA3033ED5147C13EC158B85DC75513599B0585 (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BoundingBoxDimensions::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoundingBoxDimensions__ctor_mEF132B75F7213E091DCF01A4B298C92F86E90324 (BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* __this, const RuntimeMethod* method) 
{
	{
		DimensionsBase__ctor_m9AFA3033ED5147C13EC158B85DC75513599B0585(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CellDimensions::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CellDimensions__ctor_m244B9B96048567D9FCAF6AFFCA64FEA4391EDC55 (CellDimensions_tBEFCB6EBF0FC3F93784978F73375293DAFF08EAF* __this, const RuntimeMethod* method) 
{
	{
		DimensionsBase__ctor_m9AFA3033ED5147C13EC158B85DC75513599B0585(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// BoundingBoxDimensions BoundingBox::get_Dimensions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* BoundingBox_get_Dimensions_m4F3FC84E34A2F040280CA9119B6AE2C9222F55AC (BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096* __this, const RuntimeMethod* method) 
{
	{
		// public BoundingBoxDimensions Dimensions { get; set; }
		BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* L_0 = __this->___U3CDimensionsU3Ek__BackingField_0;
		return L_0;
	}
}
// System.Void BoundingBox::set_Dimensions(BoundingBoxDimensions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoundingBox_set_Dimensions_mB6A4FF8905F4A0B36C7BFECB44B47CED60B24E0E (BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096* __this, BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* ___value0, const RuntimeMethod* method) 
{
	{
		// public BoundingBoxDimensions Dimensions { get; set; }
		BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* L_0 = ___value0;
		__this->___U3CDimensionsU3Ek__BackingField_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CDimensionsU3Ek__BackingField_0), (void*)L_0);
		return;
	}
}
// System.String BoundingBox::get_Label()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* BoundingBox_get_Label_m6C86B30C4199D01AA9838287FEA1580EA9C3DA2D (BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096* __this, const RuntimeMethod* method) 
{
	{
		// public string Label { get; set; }
		String_t* L_0 = __this->___U3CLabelU3Ek__BackingField_1;
		return L_0;
	}
}
// System.Void BoundingBox::set_Label(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoundingBox_set_Label_m30A8EF56970E2FA83C37093F48CDAE3565B0C20D (BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096* __this, String_t* ___value0, const RuntimeMethod* method) 
{
	{
		// public string Label { get; set; }
		String_t* L_0 = ___value0;
		__this->___U3CLabelU3Ek__BackingField_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CLabelU3Ek__BackingField_1), (void*)L_0);
		return;
	}
}
// System.Single BoundingBox::get_Confidence()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float BoundingBox_get_Confidence_m7930AB5EF72D84E6FCFB1432F5F62F169BB1C3A2 (BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096* __this, const RuntimeMethod* method) 
{
	{
		// public float Confidence { get; set; }
		float L_0 = __this->___U3CConfidenceU3Ek__BackingField_2;
		return L_0;
	}
}
// System.Void BoundingBox::set_Confidence(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoundingBox_set_Confidence_m4321A9367A73A6ABFF035C71565B9F42879B8F9F (BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// public float Confidence { get; set; }
		float L_0 = ___value0;
		__this->___U3CConfidenceU3Ek__BackingField_2 = L_0;
		return;
	}
}
// UnityEngine.Rect BoundingBox::get_Rect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D BoundingBox_get_Rect_m82F8A4EE4490F2EF0787A883BFA133B5D7F65C9A (BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096* __this, const RuntimeMethod* method) 
{
	{
		// get { return new Rect(Dimensions.X, Dimensions.Y, Dimensions.Width, Dimensions.Height); }
		BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* L_0;
		L_0 = BoundingBox_get_Dimensions_m4F3FC84E34A2F040280CA9119B6AE2C9222F55AC_inline(__this, NULL);
		NullCheck(L_0);
		float L_1;
		L_1 = DimensionsBase_get_X_m2AA39E00503C51C587F473D1ADBEC9CCB5D54F41_inline(L_0, NULL);
		BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* L_2;
		L_2 = BoundingBox_get_Dimensions_m4F3FC84E34A2F040280CA9119B6AE2C9222F55AC_inline(__this, NULL);
		NullCheck(L_2);
		float L_3;
		L_3 = DimensionsBase_get_Y_m170190F8D1531F544A40AB55817C1EC7EFD8A864_inline(L_2, NULL);
		BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* L_4;
		L_4 = BoundingBox_get_Dimensions_m4F3FC84E34A2F040280CA9119B6AE2C9222F55AC_inline(__this, NULL);
		NullCheck(L_4);
		float L_5;
		L_5 = DimensionsBase_get_Width_m6B20834F813A905E59F902C802FAB97D21286ECF_inline(L_4, NULL);
		BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* L_6;
		L_6 = BoundingBox_get_Dimensions_m4F3FC84E34A2F040280CA9119B6AE2C9222F55AC_inline(__this, NULL);
		NullCheck(L_6);
		float L_7;
		L_7 = DimensionsBase_get_Height_m6632C66BBCA8F5D5ABC7E95739DD3713DBA7E9BE_inline(L_6, NULL);
		Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D L_8;
		memset((&L_8), 0, sizeof(L_8));
		Rect__ctor_m18C3033D135097BEE424AAA68D91C706D2647F23((&L_8), L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.String BoundingBox::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* BoundingBox_ToString_m6DA8C8A5218D472F3B4BB8B459BDC065D937358B (BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral840F32851B594336072216EBF80BA745E8F18A4D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return $"{Label}:{Confidence}, {Dimensions.X}:{Dimensions.Y} - {Dimensions.Width}:{Dimensions.Height}";
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_0 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)SZArrayNew(ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918_il2cpp_TypeInfo_var, (uint32_t)6);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = L_0;
		String_t* L_2;
		L_2 = BoundingBox_get_Label_m6C86B30C4199D01AA9838287FEA1580EA9C3DA2D_inline(__this, NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject*)L_2);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_3 = L_1;
		float L_4;
		L_4 = BoundingBox_get_Confidence_m7930AB5EF72D84E6FCFB1432F5F62F169BB1C3A2_inline(__this, NULL);
		float L_5 = L_4;
		RuntimeObject* L_6 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject*)L_6);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_7 = L_3;
		BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* L_8;
		L_8 = BoundingBox_get_Dimensions_m4F3FC84E34A2F040280CA9119B6AE2C9222F55AC_inline(__this, NULL);
		NullCheck(L_8);
		float L_9;
		L_9 = DimensionsBase_get_X_m2AA39E00503C51C587F473D1ADBEC9CCB5D54F41_inline(L_8, NULL);
		float L_10 = L_9;
		RuntimeObject* L_11 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_11);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject*)L_11);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_12 = L_7;
		BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* L_13;
		L_13 = BoundingBox_get_Dimensions_m4F3FC84E34A2F040280CA9119B6AE2C9222F55AC_inline(__this, NULL);
		NullCheck(L_13);
		float L_14;
		L_14 = DimensionsBase_get_Y_m170190F8D1531F544A40AB55817C1EC7EFD8A864_inline(L_13, NULL);
		float L_15 = L_14;
		RuntimeObject* L_16 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_16);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject*)L_16);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_17 = L_12;
		BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* L_18;
		L_18 = BoundingBox_get_Dimensions_m4F3FC84E34A2F040280CA9119B6AE2C9222F55AC_inline(__this, NULL);
		NullCheck(L_18);
		float L_19;
		L_19 = DimensionsBase_get_Width_m6B20834F813A905E59F902C802FAB97D21286ECF_inline(L_18, NULL);
		float L_20 = L_19;
		RuntimeObject* L_21 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_21);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject*)L_21);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_22 = L_17;
		BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* L_23;
		L_23 = BoundingBox_get_Dimensions_m4F3FC84E34A2F040280CA9119B6AE2C9222F55AC_inline(__this, NULL);
		NullCheck(L_23);
		float L_24;
		L_24 = DimensionsBase_get_Height_m6632C66BBCA8F5D5ABC7E95739DD3713DBA7E9BE_inline(L_23, NULL);
		float L_25 = L_24;
		RuntimeObject* L_26 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_25);
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_26);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject*)L_26);
		String_t* L_27;
		L_27 = String_Format_m918500C1EFB475181349A79989BB79BB36102894(_stringLiteral840F32851B594336072216EBF80BA745E8F18A4D, L_22, NULL);
		return L_27;
	}
}
// System.Void BoundingBox::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoundingBox__ctor_m8E37599BC48C6E824104EC1C4A83FDA8ABA88C52 (BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PhoneCamera::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhoneCamera_Start_m750FC2C73FCF97B24D20AAB83749E2CC495E60B1 (PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.backCamera = new WebCamTexture();
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_0 = (WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749*)il2cpp_codegen_object_new(WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		WebCamTexture__ctor_mCDCF373E41263EE323147C4A50609EBE9FA28269(L_0, NULL);
		__this->___backCamera_9 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___backCamera_9), (void*)L_0);
		// this.background.texture = this.backCamera;
		RawImage_tFF12F7DB574FBDC1863CF607C7A12A5D9F8D6179* L_1 = __this->___background_12;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_2 = __this->___backCamera_9;
		NullCheck(L_1);
		RawImage_set_texture_mC016318C95CC17A826D57DD219DBCB6DFD295C02(L_1, L_2, NULL);
		// this.backCamera.Play();
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_3 = __this->___backCamera_9;
		NullCheck(L_3);
		WebCamTexture_Play_mAB313C6F98D5433C414DA31DD96316BDE8D19A26(L_3, NULL);
		// camAvailable = true;
		__this->___camAvailable_8 = (bool)1;
		// CalculateShift(Detector.IMAGE_SIZE);
		PhoneCamera_CalculateShift_mB728F0C3229A2001D59AE21D5EF675726F23F608(__this, ((int32_t)416), NULL);
		// }
		return;
	}
}
// System.Void PhoneCamera::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhoneCamera_Update_m9F6D7E0379A70DAC4A8C92DFD173B219F2707BEE (PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* __this, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	float G_B5_0 = 0.0f;
	{
		// if (!this.camAvailable)
		bool L_0 = __this->___camAvailable_8;
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// float ratio = (float)backCamera.width / (float)backCamera.height;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_1 = __this->___backCamera_9;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = VirtualFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_1);
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_3 = __this->___backCamera_9;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = VirtualFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_3);
		V_0 = ((float)(((float)L_2)/((float)L_4)));
		// fitter.aspectRatio = ratio;
		AspectRatioFitter_tEA1004A8240374C9BE7623981F999AA9789CDD09* L_5 = __this->___fitter_13;
		float L_6 = V_0;
		NullCheck(L_5);
		AspectRatioFitter_set_aspectRatio_m4192E203648BE0ACA39D9C0540C982331CEA91D9(L_5, L_6, NULL);
		// float scaleX = cameraScale;
		float L_7 = __this->___cameraScale_4;
		V_1 = L_7;
		// float scaleY = backCamera.videoVerticallyMirrored ? -cameraScale : cameraScale;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_8 = __this->___backCamera_9;
		NullCheck(L_8);
		bool L_9;
		L_9 = WebCamTexture_get_videoVerticallyMirrored_mDC7525B796A2629927EF113DA199DDE200B1B52A(L_8, NULL);
		if (L_9)
		{
			goto IL_004b;
		}
	}
	{
		float L_10 = __this->___cameraScale_4;
		G_B5_0 = L_10;
		goto IL_0052;
	}

IL_004b:
	{
		float L_11 = __this->___cameraScale_4;
		G_B5_0 = ((-L_11));
	}

IL_0052:
	{
		V_2 = G_B5_0;
		// background.rectTransform.localScale = new Vector3(scaleX, scaleY, 1f);
		RawImage_tFF12F7DB574FBDC1863CF607C7A12A5D9F8D6179* L_12 = __this->___background_12;
		NullCheck(L_12);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_13;
		L_13 = Graphic_get_rectTransform_mF4752E8934267D630810E84CE02CDFB81EB1FD6D(L_12, NULL);
		float L_14 = V_1;
		float L_15 = V_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_16;
		memset((&L_16), 0, sizeof(L_16));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_16), L_14, L_15, (1.0f), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_localScale_mBA79E811BAF6C47B80FF76414C12B47B3CD03633(L_13, L_16, NULL);
		// int orient = -backCamera.videoRotationAngle;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_17 = __this->___backCamera_9;
		NullCheck(L_17);
		int32_t L_18;
		L_18 = WebCamTexture_get_videoRotationAngle_m2BF420A1243F56415BEF82CC84AB4C7B342C991F(L_17, NULL);
		V_3 = ((-L_18));
		// background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
		RawImage_tFF12F7DB574FBDC1863CF607C7A12A5D9F8D6179* L_19 = __this->___background_12;
		NullCheck(L_19);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_20;
		L_20 = Graphic_get_rectTransform_mF4752E8934267D630810E84CE02CDFB81EB1FD6D(L_19, NULL);
		int32_t L_21 = V_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_22;
		memset((&L_22), 0, sizeof(L_22));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_22), (0.0f), (0.0f), ((float)L_21), /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_localEulerAngles_m0458551662A1A51FDCA4C0417282B25D391661DF(L_20, L_22, NULL);
		// if (orient != 0)
		int32_t L_23 = V_3;
		if (!L_23)
		{
			goto IL_00b3;
		}
	}
	{
		// this.cameraScale = (float)Screen.width / Screen.height;
		int32_t L_24;
		L_24 = Screen_get_width_mF608FF3252213E7EFA1F0D2F744C28110E9E5AC9(NULL);
		int32_t L_25;
		L_25 = Screen_get_height_m01A3102DE71EE1FBEA51D09D6B0261CF864FE8F9(NULL);
		__this->___cameraScale_4 = ((float)(((float)L_24)/((float)L_25)));
	}

IL_00b3:
	{
		// TFDetect();
		PhoneCamera_TFDetect_mFF76488480372773AF1F894433E4DAC1FFCB8E80(__this, NULL);
		// }
		return;
	}
}
// System.Void PhoneCamera::CalculateShift(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhoneCamera_CalculateShift_mB728F0C3229A2001D59AE21D5EF675726F23F608 (PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* __this, int32_t ___inputSize0, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		// if (Screen.width < Screen.height)
		int32_t L_0;
		L_0 = Screen_get_width_mF608FF3252213E7EFA1F0D2F744C28110E9E5AC9(NULL);
		int32_t L_1;
		L_1 = Screen_get_height_m01A3102DE71EE1FBEA51D09D6B0261CF864FE8F9(NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0028;
		}
	}
	{
		// smallest = Screen.width;
		int32_t L_2;
		L_2 = Screen_get_width_mF608FF3252213E7EFA1F0D2F744C28110E9E5AC9(NULL);
		V_0 = L_2;
		// this.shiftY = (Screen.height - smallest) / 2f;
		int32_t L_3;
		L_3 = Screen_get_height_m01A3102DE71EE1FBEA51D09D6B0261CF864FE8F9(NULL);
		int32_t L_4 = V_0;
		__this->___shiftY_6 = ((float)(((float)((int32_t)il2cpp_codegen_subtract(L_3, L_4)))/(2.0f)));
		goto IL_0042;
	}

IL_0028:
	{
		// smallest = Screen.height;
		int32_t L_5;
		L_5 = Screen_get_height_m01A3102DE71EE1FBEA51D09D6B0261CF864FE8F9(NULL);
		V_0 = L_5;
		// this.shiftX = (Screen.width - smallest) / 2f;
		int32_t L_6;
		L_6 = Screen_get_width_mF608FF3252213E7EFA1F0D2F744C28110E9E5AC9(NULL);
		int32_t L_7 = V_0;
		__this->___shiftX_5 = ((float)(((float)((int32_t)il2cpp_codegen_subtract(L_6, L_7)))/(2.0f)));
	}

IL_0042:
	{
		// this.scaleFactor = smallest / (float)inputSize;
		int32_t L_8 = V_0;
		int32_t L_9 = ___inputSize0;
		__this->___scaleFactor_7 = ((float)(((float)L_8)/((float)L_9)));
		// }
		return;
	}
}
// System.Void PhoneCamera::TFDetect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhoneCamera_TFDetect_mFF76488480372773AF1F894433E4DAC1FFCB8E80 (PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PhoneCamera_U3CTFDetectU3Eb__13_0_m8C397BA0EB24746D6967E6F04C682C86B540BD48_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (this.isWorking)
		bool L_0 = __this->___isWorking_10;
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// this.isWorking = true;
		__this->___isWorking_10 = (bool)1;
		// StartCoroutine(ProcessImage(Detector.IMAGE_SIZE, result =>
		// {
		//     StartCoroutine(this.detector.Detect(result, boxes =>
		//     {
		//         //this.boxOutlines = boxes;
		//         Resources.UnloadUnusedAssets();
		//         this.isWorking = false;
		//     }));
		// }));
		Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9* L_1 = (Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9*)il2cpp_codegen_object_new(Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		Action_1__ctor_m9A437718C386CE2D3AC7049893F4C00BA6CDE990(L_1, __this, (intptr_t)((void*)PhoneCamera_U3CTFDetectU3Eb__13_0_m8C397BA0EB24746D6967E6F04C682C86B540BD48_RuntimeMethod_var), NULL);
		RuntimeObject* L_2;
		L_2 = PhoneCamera_ProcessImage_m68C34D4C3E9C403AE677F921D15E7B5D06EF650E(__this, ((int32_t)416), L_1, NULL);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_3;
		L_3 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(__this, L_2, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator PhoneCamera::ProcessImage(System.Int32,System.Action`1<UnityEngine.Color32[]>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PhoneCamera_ProcessImage_m68C34D4C3E9C403AE677F921D15E7B5D06EF650E (PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* __this, int32_t ___inputSize0, Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9* ___callback1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3* L_0 = (U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3*)il2cpp_codegen_object_new(U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CProcessImageU3Ed__14__ctor_m64F65AE69E02E4D6DA72E4A43FD9E2E4D712C584(L_0, 0, NULL);
		U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3* L_2 = L_1;
		int32_t L_3 = ___inputSize0;
		NullCheck(L_2);
		L_2->___inputSize_3 = L_3;
		U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3* L_4 = L_2;
		Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9* L_5 = ___callback1;
		NullCheck(L_4);
		L_4->___callback_4 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&L_4->___callback_4), (void*)L_5);
		return L_4;
	}
}
// UnityEngine.Texture2D PhoneCamera::Scale(UnityEngine.Texture2D,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* PhoneCamera_Scale_m9A8BA55FFE200580A45E79CAD587A84A5CA54908 (PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* __this, Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___texture0, int32_t ___imageSize1, const RuntimeMethod* method) 
{
	{
		// var scaled = TextureTools.scaled(texture, imageSize, imageSize, FilterMode.Bilinear);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_0 = ___texture0;
		int32_t L_1 = ___imageSize1;
		int32_t L_2 = ___imageSize1;
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_3;
		L_3 = TextureTools_scaled_m42BE122AF1DC9EBC821B957472F4E956F8395885(L_0, L_1, L_2, 1, NULL);
		// return scaled;
		return L_3;
	}
}
// UnityEngine.Color32[] PhoneCamera::Rotate(UnityEngine.Color32[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* PhoneCamera_Rotate_m52B13D25E4B4F2222D4B09FDF6E68D4F38CDB84A (PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* __this, Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___pixels0, int32_t ___width1, int32_t ___height2, const RuntimeMethod* method) 
{
	{
		// return TextureTools.RotateImageMatrix(pixels, width, height, -90);                //-90
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_0 = ___pixels0;
		int32_t L_1 = ___width1;
		int32_t L_2 = ___height2;
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_3;
		L_3 = TextureTools_RotateImageMatrix_m6042CB8FC50C973D9A2D1D1DBE53E21435BF073B(L_0, L_1, L_2, ((int32_t)-90), NULL);
		return L_3;
	}
}
// System.Void PhoneCamera::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhoneCamera__ctor_mBE881DC2D28EFE418D69A390D74C70C532F380C3 (PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* __this, const RuntimeMethod* method) 
{
	{
		// private float cameraScale = 1f;
		__this->___cameraScale_4 = (1.0f);
		// private float scaleFactor = 1;
		__this->___scaleFactor_7 = (1.0f);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void PhoneCamera::<TFDetect>b__13_0(UnityEngine.Color32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhoneCamera_U3CTFDetectU3Eb__13_0_m8C397BA0EB24746D6967E6F04C682C86B540BD48 (PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* __this, Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___result0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t267C05F8874B612902659095FC16875EF00A902D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PhoneCamera_U3CTFDetectU3Eb__13_1_mAD506FF440893CFFD070C52A1CBBDFF359DB3F51_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// StartCoroutine(this.detector.Detect(result, boxes =>
		// {
		//     //this.boxOutlines = boxes;
		//     Resources.UnloadUnusedAssets();
		//     this.isWorking = false;
		// }));
		Detector_t4563441D0E64FB255B421A325E6916CB73C0A323* L_0 = __this->___detector_11;
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_1 = ___result0;
		Action_1_t267C05F8874B612902659095FC16875EF00A902D* L_2 = (Action_1_t267C05F8874B612902659095FC16875EF00A902D*)il2cpp_codegen_object_new(Action_1_t267C05F8874B612902659095FC16875EF00A902D_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		Action_1__ctor_mE843E93E00C081B2E44D3AFCF595129033F8FA64(L_2, __this, (intptr_t)((void*)PhoneCamera_U3CTFDetectU3Eb__13_1_mAD506FF440893CFFD070C52A1CBBDFF359DB3F51_RuntimeMethod_var), NULL);
		NullCheck(L_0);
		RuntimeObject* L_3;
		L_3 = Detector_Detect_mF7E06C6A16E6005CBA62AC5BBD89CABD3032E12E(L_0, L_1, L_2, NULL);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_4;
		L_4 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(__this, L_3, NULL);
		// }));
		return;
	}
}
// System.Void PhoneCamera::<TFDetect>b__13_1(System.Collections.Generic.IList`1<BoundingBox>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhoneCamera_U3CTFDetectU3Eb__13_1_mAD506FF440893CFFD070C52A1CBBDFF359DB3F51 (PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* __this, RuntimeObject* ___boxes0, const RuntimeMethod* method) 
{
	{
		// Resources.UnloadUnusedAssets();
		AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C* L_0;
		L_0 = Resources_UnloadUnusedAssets_m4003CD3EBC3AC2738DE9F2960D5BC45818C1F12B(NULL);
		// this.isWorking = false;
		__this->___isWorking_10 = (bool)0;
		// }));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PhoneCamera/<>c__DisplayClass14_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_0__ctor_mBCB0D3117D67C026EBB00245AFD9AB9A7E8962C8 (U3CU3Ec__DisplayClass14_0_tFF5B9D2B76EB8F350A918BA71C6925E798589AA8* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void PhoneCamera/<>c__DisplayClass14_0::<ProcessImage>b__0(UnityEngine.Texture2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_0_U3CProcessImageU3Eb__0_m3E7C0BDCC16F0E5A81D725A7A4547CE8DE688FB5 (U3CU3Ec__DisplayClass14_0_tFF5B9D2B76EB8F350A918BA71C6925E798589AA8* __this, Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___snap0, const RuntimeMethod* method) 
{
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* V_0 = NULL;
	Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* V_1 = NULL;
	{
		// var scaled = Scale(snap, inputSize);
		PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* L_0 = __this->___U3CU3E4__this_0;
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_1 = ___snap0;
		int32_t L_2 = __this->___inputSize_1;
		NullCheck(L_0);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_3;
		L_3 = PhoneCamera_Scale_m9A8BA55FFE200580A45E79CAD587A84A5CA54908(L_0, L_1, L_2, NULL);
		V_0 = L_3;
		// var rotated = Rotate(scaled.GetPixels32(), scaled.width, scaled.height);
		PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* L_4 = __this->___U3CU3E4__this_0;
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_5 = V_0;
		NullCheck(L_5);
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_6;
		L_6 = Texture2D_GetPixels32_m48230192E7543765C1A517F251D1D9BFCFB58C3D(L_5, NULL);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8;
		L_8 = VirtualFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_7);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10;
		L_10 = VirtualFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_9);
		NullCheck(L_4);
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_11;
		L_11 = PhoneCamera_Rotate_m52B13D25E4B4F2222D4B09FDF6E68D4F38CDB84A(L_4, L_6, L_8, L_10, NULL);
		V_1 = L_11;
		// callback(rotated);
		Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9* L_12 = __this->___callback_2;
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_13 = V_1;
		NullCheck(L_12);
		Action_1_Invoke_mA379E47BAF47659F172826042396C270CD777D00_inline(L_12, L_13, NULL);
		// }));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PhoneCamera/<ProcessImage>d__14::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessImageU3Ed__14__ctor_m64F65AE69E02E4D6DA72E4A43FD9E2E4D712C584 (U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void PhoneCamera/<ProcessImage>d__14::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessImageU3Ed__14_System_IDisposable_Dispose_mD3B8D74F60C018E854202415357CA5496FBD720D (U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean PhoneCamera/<ProcessImage>d__14::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CProcessImageU3Ed__14_MoveNext_m7D790D0E94C4F240CFB990248065EE3640AC4D64 (U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_tD7F74291FAA5A362F4A5F48D4E1CF3ED51FA6A33_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass14_0_U3CProcessImageU3Eb__0_m3E7C0BDCC16F0E5A81D725A7A4547CE8DE688FB5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass14_0_tFF5B9D2B76EB8F350A918BA71C6925E798589AA8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* V_1 = NULL;
	U3CU3Ec__DisplayClass14_0_tFF5B9D2B76EB8F350A918BA71C6925E798589AA8* V_2 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0075;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		U3CU3Ec__DisplayClass14_0_tFF5B9D2B76EB8F350A918BA71C6925E798589AA8* L_4 = (U3CU3Ec__DisplayClass14_0_tFF5B9D2B76EB8F350A918BA71C6925E798589AA8*)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass14_0_tFF5B9D2B76EB8F350A918BA71C6925E798589AA8_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		U3CU3Ec__DisplayClass14_0__ctor_mBCB0D3117D67C026EBB00245AFD9AB9A7E8962C8(L_4, NULL);
		V_2 = L_4;
		U3CU3Ec__DisplayClass14_0_tFF5B9D2B76EB8F350A918BA71C6925E798589AA8* L_5 = V_2;
		PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* L_6 = __this->___U3CU3E4__this_2;
		NullCheck(L_5);
		L_5->___U3CU3E4__this_0 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&L_5->___U3CU3E4__this_0), (void*)L_6);
		U3CU3Ec__DisplayClass14_0_tFF5B9D2B76EB8F350A918BA71C6925E798589AA8* L_7 = V_2;
		int32_t L_8 = __this->___inputSize_3;
		NullCheck(L_7);
		L_7->___inputSize_1 = L_8;
		U3CU3Ec__DisplayClass14_0_tFF5B9D2B76EB8F350A918BA71C6925E798589AA8* L_9 = V_2;
		Action_1_t913C941CAC508E9B0879F55119E52946D35F94E9* L_10 = __this->___callback_4;
		NullCheck(L_9);
		L_9->___callback_2 = L_10;
		Il2CppCodeGenWriteBarrier((void**)(&L_9->___callback_2), (void*)L_10);
		// yield return StartCoroutine(TextureTools.CropSquare(backCamera,
		//     TextureTools.RectOptions.Center, snap =>
		//     {
		//         var scaled = Scale(snap, inputSize);
		//         var rotated = Rotate(scaled.GetPixels32(), scaled.width, scaled.height);
		//         callback(rotated);
		// 
		//     }));
		PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* L_11 = V_1;
		PhoneCamera_t470079F18356FD3BAB475C5EC57AEC8302DCFFC4* L_12 = V_1;
		NullCheck(L_12);
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_13 = L_12->___backCamera_9;
		U3CU3Ec__DisplayClass14_0_tFF5B9D2B76EB8F350A918BA71C6925E798589AA8* L_14 = V_2;
		Action_1_tD7F74291FAA5A362F4A5F48D4E1CF3ED51FA6A33* L_15 = (Action_1_tD7F74291FAA5A362F4A5F48D4E1CF3ED51FA6A33*)il2cpp_codegen_object_new(Action_1_tD7F74291FAA5A362F4A5F48D4E1CF3ED51FA6A33_il2cpp_TypeInfo_var);
		NullCheck(L_15);
		Action_1__ctor_m0D74126D5BBFFF417FDA1881B8AEBD833A833F76(L_15, L_14, (intptr_t)((void*)U3CU3Ec__DisplayClass14_0_U3CProcessImageU3Eb__0_m3E7C0BDCC16F0E5A81D725A7A4547CE8DE688FB5_RuntimeMethod_var), NULL);
		RuntimeObject* L_16;
		L_16 = TextureTools_CropSquare_m8CFFF250CD0D934710F6D15DE737D91AAE29B96E(L_13, 0, L_15, NULL);
		NullCheck(L_11);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_17;
		L_17 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(L_11, L_16, NULL);
		__this->___U3CU3E2__current_1 = L_17;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_17);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0075:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// }
		return (bool)0;
	}
}
// System.Object PhoneCamera/<ProcessImage>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CProcessImageU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m322184F6BAFE67C5FD9B78B68BBD8521BA4C8E6C (U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void PhoneCamera/<ProcessImage>d__14::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessImageU3Ed__14_System_Collections_IEnumerator_Reset_mF3E80C2683A1452A45277CD2FA554C0AE7CC6B89 (U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CProcessImageU3Ed__14_System_Collections_IEnumerator_Reset_mF3E80C2683A1452A45277CD2FA554C0AE7CC6B89_RuntimeMethod_var)));
	}
}
// System.Object PhoneCamera/<ProcessImage>d__14::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CProcessImageU3Ed__14_System_Collections_IEnumerator_get_Current_mCD5396DFDE91EA5F055C2D1DE9399BD8245C19FE (U3CProcessImageU3Ed__14_t72E1CA558B33B8EA5438AA4E2B22DE32C7FA6BC3* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.IEnumerator TFClassify.TextureTools::CropSquare(UnityEngine.WebCamTexture,TFClassify.TextureTools/RectOptions,System.Action`1<UnityEngine.Texture2D>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TextureTools_CropSquare_m8CFFF250CD0D934710F6D15DE737D91AAE29B96E (WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* ___texture0, int32_t ___rectOptions1, Action_1_tD7F74291FAA5A362F4A5F48D4E1CF3ED51FA6A33* ___callback2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F* L_0 = (U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F*)il2cpp_codegen_object_new(U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CCropSquareU3Ed__2__ctor_mFD8FDCD0E558E01DE3C96314D12284483696A1B3(L_0, 0, NULL);
		U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F* L_1 = L_0;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_2 = ___texture0;
		NullCheck(L_1);
		L_1->___texture_2 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___texture_2), (void*)L_2);
		U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F* L_3 = L_1;
		int32_t L_4 = ___rectOptions1;
		NullCheck(L_3);
		L_3->___rectOptions_3 = L_4;
		U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F* L_5 = L_3;
		Action_1_tD7F74291FAA5A362F4A5F48D4E1CF3ED51FA6A33* L_6 = ___callback2;
		NullCheck(L_5);
		L_5->___callback_4 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&L_5->___callback_4), (void*)L_6);
		return L_5;
	}
}
// UnityEngine.Texture2D TFClassify.TextureTools::scaled(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.FilterMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* TextureTools_scaled_m42BE122AF1DC9EBC821B957472F4E956F8395885 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___src0, int32_t ___width1, int32_t ___height2, int32_t ___mode3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Rect texR = new Rect(0,0,width,height);
		int32_t L_0 = ___width1;
		int32_t L_1 = ___height2;
		Rect__ctor_m18C3033D135097BEE424AAA68D91C706D2647F23((&V_0), (0.0f), (0.0f), ((float)L_0), ((float)L_1), NULL);
		// _gpu_scale(src,width,height,mode);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_2 = ___src0;
		int32_t L_3 = ___width1;
		int32_t L_4 = ___height2;
		int32_t L_5 = ___mode3;
		TextureTools__gpu_scale_m41EB6FEFF1926369E00E6254F6F07C2B8BEDE8DF(L_2, L_3, L_4, L_5, NULL);
		// Texture2D result = new Texture2D(width, height, TextureFormat.ARGB32, true);
		int32_t L_6 = ___width1;
		int32_t L_7 = ___height2;
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_8 = (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4*)il2cpp_codegen_object_new(Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var);
		NullCheck(L_8);
		Texture2D__ctor_mECF60A9EC0638EC353C02C8E99B6B465D23BE917(L_8, L_6, L_7, 5, (bool)1, NULL);
		// result.Reinitialize(width, height);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_9 = L_8;
		int32_t L_10 = ___width1;
		int32_t L_11 = ___height2;
		NullCheck(L_9);
		bool L_12;
		L_12 = Texture2D_Reinitialize_m9AB4169DA359C18BB4102F8E00C4321B53714E6B(L_9, L_10, L_11, NULL);
		// result.ReadPixels(texR,0,0,true);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_13 = L_9;
		Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D L_14 = V_0;
		NullCheck(L_13);
		Texture2D_ReadPixels_m7483DB211233F02E46418E9A6077487925F0024C(L_13, L_14, 0, 0, (bool)1, NULL);
		// return result;
		return L_13;
	}
}
// System.Void TFClassify.TextureTools::_gpu_scale(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.FilterMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextureTools__gpu_scale_m41EB6FEFF1926369E00E6254F6F07C2B8BEDE8DF (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___src0, int32_t ___width1, int32_t ___height2, int32_t ___fmode3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// src.filterMode = fmode;
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_0 = ___src0;
		int32_t L_1 = ___fmode3;
		NullCheck(L_0);
		Texture_set_filterMode_mE423E58C0C16D059EA62BA87AD70F44AEA50CCC9(L_0, L_1, NULL);
		// src.Apply(true);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_2 = ___src0;
		NullCheck(L_2);
		Texture2D_Apply_mCC369BCAB2D3AD3EE50EE01DA67AF227865FA2B3(L_2, (bool)1, NULL);
		// RenderTexture rtt = new RenderTexture(width, height, 32);
		int32_t L_3 = ___width1;
		int32_t L_4 = ___height2;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_5 = (RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27*)il2cpp_codegen_object_new(RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		RenderTexture__ctor_m45EACC89DDF408948889586516B3CA7AA8B73BFA(L_5, L_3, L_4, ((int32_t)32), NULL);
		// Graphics.SetRenderTarget(rtt);
		il2cpp_codegen_runtime_class_init_inline(Graphics_t99CD970FFEA58171C70F54DF0C06D315BD452F2C_il2cpp_TypeInfo_var);
		Graphics_SetRenderTarget_m995C0F14B97C5BF46CCF2E7EF410C1CC05C46409(L_5, NULL);
		// GL.LoadPixelMatrix(0,1,1,0);
		GL_LoadPixelMatrix_mF1C5A4508C5F110512C116A5DDE7AB0483FE961A((0.0f), (1.0f), (1.0f), (0.0f), NULL);
		// GL.Clear(true,true,new Color(0,0,0,0));
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_6;
		memset((&L_6), 0, sizeof(L_6));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_6), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		GL_Clear_mA172E771FC32B516DB826F537832307C3A16BE09((bool)1, (bool)1, L_6, NULL);
		// Graphics.DrawTexture(new Rect(0,0,1,1),src);
		Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D L_7;
		memset((&L_7), 0, sizeof(L_7));
		Rect__ctor_m18C3033D135097BEE424AAA68D91C706D2647F23((&L_7), (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_8 = ___src0;
		Graphics_DrawTexture_m400F92CB13445A7BC054BC074B7073EA7E4B322F(L_7, L_8, NULL);
		// }
		return;
	}
}
// UnityEngine.Color32[] TFClassify.TextureTools::RotateImageMatrix(UnityEngine.Color32[],System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* TextureTools_RotateImageMatrix_m6042CB8FC50C973D9A2D1D1DBE53E21435BF073B (Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___matrix0, int32_t ___width1, int32_t ___height2, int32_t ___angle3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		// Color32[] pix1 = new Color32[matrix.Length];
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_0 = ___matrix0;
		NullCheck(L_0);
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_1 = (Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259*)(Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259*)SZArrayNew(Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259_il2cpp_TypeInfo_var, (uint32_t)((int32_t)(((RuntimeArray*)L_0)->max_length)));
		V_0 = L_1;
		// int x = 0;
		V_1 = 0;
		// int y = 0;
		V_2 = 0;
		// Color32[] pix3 = rotateSquare(
		//     matrix, width, height, (Math.PI/180*(double)angle));
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_2 = ___matrix0;
		int32_t L_3 = ___width1;
		int32_t L_4 = ___height2;
		int32_t L_5 = ___angle3;
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_6;
		L_6 = TextureTools_rotateSquare_m39D669E9FC88B16402F908880E5796BA7749E2DF(L_2, L_3, L_4, ((double)il2cpp_codegen_multiply((0.017453292519943295), ((double)L_5))), NULL);
		V_3 = L_6;
		// for (int j = 0; j < height; j++){
		V_4 = 0;
		goto IL_005b;
	}

IL_0027:
	{
		// for (var i = 0; i < width; i++) {
		V_5 = 0;
		goto IL_0050;
	}

IL_002c:
	{
		// pix1[x + i + width*(j+y)] = pix3[i + j*width];
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_7 = V_0;
		int32_t L_8 = V_1;
		int32_t L_9 = V_5;
		int32_t L_10 = ___width1;
		int32_t L_11 = V_4;
		int32_t L_12 = V_2;
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_13 = V_3;
		int32_t L_14 = V_5;
		int32_t L_15 = V_4;
		int32_t L_16 = ___width1;
		NullCheck(L_13);
		int32_t L_17 = ((int32_t)il2cpp_codegen_add(L_14, ((int32_t)il2cpp_codegen_multiply(L_15, L_16))));
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_18 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_add(L_8, L_9)), ((int32_t)il2cpp_codegen_multiply(L_10, ((int32_t)il2cpp_codegen_add(L_11, L_12))))))), (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B)L_18);
		// for (var i = 0; i < width; i++) {
		int32_t L_19 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add(L_19, 1));
	}

IL_0050:
	{
		// for (var i = 0; i < width; i++) {
		int32_t L_20 = V_5;
		int32_t L_21 = ___width1;
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_002c;
		}
	}
	{
		// for (int j = 0; j < height; j++){
		int32_t L_22 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_22, 1));
	}

IL_005b:
	{
		// for (int j = 0; j < height; j++){
		int32_t L_23 = V_4;
		int32_t L_24 = ___height2;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0027;
		}
	}
	{
		// return pix3;
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_25 = V_3;
		return L_25;
	}
}
// UnityEngine.Color32[] TFClassify.TextureTools::rotateSquare(UnityEngine.Color32[],System.Int32,System.Int32,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* TextureTools_rotateSquare_m39D669E9FC88B16402F908880E5796BA7749E2DF (Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* ___arr0, int32_t ___width1, int32_t ___height2, double ___phi3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	double V_4 = 0.0;
	double V_5 = 0.0;
	Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* V_6 = NULL;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		// double sn = Math.Sin(phi);
		double L_0 = ___phi3;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = sin(L_0);
		V_4 = L_1;
		// double cs = Math.Cos(phi);
		double L_2 = ___phi3;
		double L_3;
		L_3 = cos(L_2);
		V_5 = L_3;
		// Color32[] arr2 = new Color32[arr.Length];
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_4 = ___arr0;
		NullCheck(L_4);
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_5 = (Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259*)(Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259*)SZArrayNew(Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259_il2cpp_TypeInfo_var, (uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length)));
		V_6 = L_5;
		// int xc = width/2;
		int32_t L_6 = ___width1;
		V_7 = ((int32_t)(L_6/2));
		// int yc = height/2;
		int32_t L_7 = ___height2;
		V_8 = ((int32_t)(L_7/2));
		// for (j=0; j<height; j++){
		V_3 = 0;
		goto IL_00a3;
	}

IL_0028:
	{
		// for (i=0; i<width; i++){
		V_2 = 0;
		goto IL_009b;
	}

IL_002c:
	{
		// arr2[j*width+i] = new Color32(0,0,0,0);
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_8 = V_6;
		int32_t L_9 = V_3;
		int32_t L_10 = ___width1;
		int32_t L_11 = V_2;
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_12;
		memset((&L_12), 0, sizeof(L_12));
		Color32__ctor_mC9C6B443F0C7CA3F8B174158B2AF6F05E18EAC4E_inline((&L_12), (uint8_t)0, (uint8_t)0, (uint8_t)0, (uint8_t)0, /*hidden argument*/NULL);
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_9, L_10)), L_11))), (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B)L_12);
		// x = (int)(cs*(i-xc)+sn*(j-yc)+xc);
		double L_13 = V_5;
		int32_t L_14 = V_2;
		int32_t L_15 = V_7;
		double L_16 = V_4;
		int32_t L_17 = V_3;
		int32_t L_18 = V_8;
		int32_t L_19 = V_7;
		V_0 = il2cpp_codegen_cast_double_to_int<int32_t>(((double)il2cpp_codegen_add(((double)il2cpp_codegen_add(((double)il2cpp_codegen_multiply(L_13, ((double)((int32_t)il2cpp_codegen_subtract(L_14, L_15))))), ((double)il2cpp_codegen_multiply(L_16, ((double)((int32_t)il2cpp_codegen_subtract(L_17, L_18))))))), ((double)L_19))));
		// y = (int)(-sn*(i-xc)+cs*(j-yc)+yc);
		double L_20 = V_4;
		int32_t L_21 = V_2;
		int32_t L_22 = V_7;
		double L_23 = V_5;
		int32_t L_24 = V_3;
		int32_t L_25 = V_8;
		int32_t L_26 = V_8;
		V_1 = il2cpp_codegen_cast_double_to_int<int32_t>(((double)il2cpp_codegen_add(((double)il2cpp_codegen_add(((double)il2cpp_codegen_multiply(((-L_20)), ((double)((int32_t)il2cpp_codegen_subtract(L_21, L_22))))), ((double)il2cpp_codegen_multiply(L_23, ((double)((int32_t)il2cpp_codegen_subtract(L_24, L_25))))))), ((double)L_26))));
		// if ((x>-1) && (x<width) &&(y>-1) && (y<height)){
		int32_t L_27 = V_0;
		if ((((int32_t)L_27) <= ((int32_t)(-1))))
		{
			goto IL_0097;
		}
	}
	{
		int32_t L_28 = V_0;
		int32_t L_29 = ___width1;
		if ((((int32_t)L_28) >= ((int32_t)L_29)))
		{
			goto IL_0097;
		}
	}
	{
		int32_t L_30 = V_1;
		if ((((int32_t)L_30) <= ((int32_t)(-1))))
		{
			goto IL_0097;
		}
	}
	{
		int32_t L_31 = V_1;
		int32_t L_32 = ___height2;
		if ((((int32_t)L_31) >= ((int32_t)L_32)))
		{
			goto IL_0097;
		}
	}
	{
		// arr2[j*width+i]=arr[y*width+x];
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_33 = V_6;
		int32_t L_34 = V_3;
		int32_t L_35 = ___width1;
		int32_t L_36 = V_2;
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_37 = ___arr0;
		int32_t L_38 = V_1;
		int32_t L_39 = ___width1;
		int32_t L_40 = V_0;
		NullCheck(L_37);
		int32_t L_41 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_38, L_39)), L_40));
		Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B L_42 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		NullCheck(L_33);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_34, L_35)), L_36))), (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B)L_42);
	}

IL_0097:
	{
		// for (i=0; i<width; i++){
		int32_t L_43 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_43, 1));
	}

IL_009b:
	{
		// for (i=0; i<width; i++){
		int32_t L_44 = V_2;
		int32_t L_45 = ___width1;
		if ((((int32_t)L_44) < ((int32_t)L_45)))
		{
			goto IL_002c;
		}
	}
	{
		// for (j=0; j<height; j++){
		int32_t L_46 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_46, 1));
	}

IL_00a3:
	{
		// for (j=0; j<height; j++){
		int32_t L_47 = V_3;
		int32_t L_48 = ___height2;
		if ((((int32_t)L_47) < ((int32_t)L_48)))
		{
			goto IL_0028;
		}
	}
	{
		// return arr2;
		Color32U5BU5D_t38116C3E91765C4C5726CE12C77FAD7F9F737259* L_49 = V_6;
		return L_49;
	}
}
// System.Void TFClassify.TextureTools::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextureTools__ctor_mFCE1EC2FA0A438A35626FE5FA3CA479A79F5A109 (TextureTools_t4375AA99E42B9E4DE7491662E55DD0F502341F5F* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TFClassify.TextureTools/<CropSquare>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCropSquareU3Ed__2__ctor_mFD8FDCD0E558E01DE3C96314D12284483696A1B3 (U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void TFClassify.TextureTools/<CropSquare>d__2::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCropSquareU3Ed__2_System_IDisposable_Dispose_m9071D08371C1CB5BC70AF7711C91EB59EDB02BDA (U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean TFClassify.TextureTools/<CropSquare>d__2::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CCropSquareU3Ed__2_MoveNext_mC58C96CCC338EA44F5D3BA39E81E547C28E15094 (U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D V_2;
	memset((&V_2), 0, sizeof(V_2));
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	int32_t V_9 = 0;
	int32_t G_B5_0 = 0;
	float G_B19_0 = 0.0f;
	float G_B22_0 = 0.0f;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_030c;
			}
			case 2:
			{
				goto IL_032e;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// var smallest = texture.width < texture.height ? texture.width : texture.height;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_2 = __this->___texture_2;
		NullCheck(L_2);
		int32_t L_3;
		L_3 = VirtualFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_2);
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_4 = __this->___texture_2;
		NullCheck(L_4);
		int32_t L_5;
		L_5 = VirtualFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_4);
		if ((((int32_t)L_3) < ((int32_t)L_5)))
		{
			goto IL_0047;
		}
	}
	{
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_6 = __this->___texture_2;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = VirtualFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_6);
		G_B5_0 = L_7;
		goto IL_0052;
	}

IL_0047:
	{
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_8 = __this->___texture_2;
		NullCheck(L_8);
		int32_t L_9;
		L_9 = VirtualFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_8);
		G_B5_0 = L_9;
	}

IL_0052:
	{
		V_1 = G_B5_0;
		// var rect = new Rect(0, 0, smallest, smallest);
		int32_t L_10 = V_1;
		int32_t L_11 = V_1;
		Rect__ctor_m18C3033D135097BEE424AAA68D91C706D2647F23((&V_2), (0.0f), (0.0f), ((float)L_10), ((float)L_11), NULL);
		// if(rect.height < 0 || rect.width < 0)
		float L_12;
		L_12 = Rect_get_height_mE1AA6C6C725CCD2D317BD2157396D3CF7D47C9D8((&V_2), NULL);
		if ((((float)L_12) < ((float)(0.0f))))
		{
			goto IL_0084;
		}
	}
	{
		float L_13;
		L_13 = Rect_get_width_m620D67551372073C9C32C4C4624C2A5713F7F9A9((&V_2), NULL);
		if ((!(((float)L_13) < ((float)(0.0f)))))
		{
			goto IL_008f;
		}
	}

IL_0084:
	{
		// throw new ArgumentException("Invalid texture size");
		ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* L_14 = (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var)));
		NullCheck(L_14);
		ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465(L_14, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral67086B015EBA786480C6ECD1FC5F51EDB80A5A0E)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCropSquareU3Ed__2_MoveNext_mC58C96CCC338EA44F5D3BA39E81E547C28E15094_RuntimeMethod_var)));
	}

IL_008f:
	{
		// Texture2D result = new Texture2D((int)rect.width, (int)rect.height);
		float L_15;
		L_15 = Rect_get_width_m620D67551372073C9C32C4C4624C2A5713F7F9A9((&V_2), NULL);
		float L_16;
		L_16 = Rect_get_height_mE1AA6C6C725CCD2D317BD2157396D3CF7D47C9D8((&V_2), NULL);
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_17 = (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4*)il2cpp_codegen_object_new(Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var);
		NullCheck(L_17);
		Texture2D__ctor_m3BA82E87442B7F69E118477069AE11101B9DF796(L_17, il2cpp_codegen_cast_double_to_int<int32_t>(L_15), il2cpp_codegen_cast_double_to_int<int32_t>(L_16), NULL);
		__this->___U3CresultU3E5__2_5 = L_17;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CresultU3E5__2_5), (void*)L_17);
		// if(rect.width != 0 && rect.height != 0)
		float L_18;
		L_18 = Rect_get_width_m620D67551372073C9C32C4C4624C2A5713F7F9A9((&V_2), NULL);
		if ((((float)L_18) == ((float)(0.0f))))
		{
			goto IL_031e;
		}
	}
	{
		float L_19;
		L_19 = Rect_get_height_mE1AA6C6C725CCD2D317BD2157396D3CF7D47C9D8((&V_2), NULL);
		if ((((float)L_19) == ((float)(0.0f))))
		{
			goto IL_031e;
		}
	}
	{
		// float xRect = rect.x;
		float L_20;
		L_20 = Rect_get_x_mB267B718E0D067F2BAE31BA477647FBF964916EB((&V_2), NULL);
		V_3 = L_20;
		// float yRect = rect.y;
		float L_21;
		L_21 = Rect_get_y_mC733E8D49F3CE21B2A3D40A1B72D687F22C97F49((&V_2), NULL);
		V_4 = L_21;
		// float widthRect = rect.width;
		float L_22;
		L_22 = Rect_get_width_m620D67551372073C9C32C4C4624C2A5713F7F9A9((&V_2), NULL);
		V_5 = L_22;
		// float heightRect = rect.height;
		float L_23;
		L_23 = Rect_get_height_mE1AA6C6C725CCD2D317BD2157396D3CF7D47C9D8((&V_2), NULL);
		V_6 = L_23;
		int32_t L_24 = __this->___rectOptions_3;
		V_9 = L_24;
		int32_t L_25 = V_9;
		switch (L_25)
		{
			case 0:
			{
				goto IL_012b;
			}
			case 1:
			{
				goto IL_0167;
			}
			case 2:
			{
				goto IL_019c;
			}
			case 3:
			{
				goto IL_022a;
			}
			case 4:
			{
				goto IL_0181;
			}
			case 5:
			{
				goto IL_022a;
			}
			case 6:
			{
				goto IL_022a;
			}
			case 7:
			{
				goto IL_022a;
			}
			case 8:
			{
				goto IL_022a;
			}
			case 9:
			{
				goto IL_01c9;
			}
		}
	}
	{
		goto IL_022a;
	}

IL_012b:
	{
		// xRect = (texture.width - rect.width) / 2;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_26 = __this->___texture_2;
		NullCheck(L_26);
		int32_t L_27;
		L_27 = VirtualFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_26);
		float L_28;
		L_28 = Rect_get_width_m620D67551372073C9C32C4C4624C2A5713F7F9A9((&V_2), NULL);
		V_3 = ((float)(((float)il2cpp_codegen_subtract(((float)L_27), L_28))/(2.0f)));
		// yRect = (texture.height - rect.height) / 2;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_29 = __this->___texture_2;
		NullCheck(L_29);
		int32_t L_30;
		L_30 = VirtualFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_29);
		float L_31;
		L_31 = Rect_get_height_mE1AA6C6C725CCD2D317BD2157396D3CF7D47C9D8((&V_2), NULL);
		V_4 = ((float)(((float)il2cpp_codegen_subtract(((float)L_30), L_31))/(2.0f)));
		// break;
		goto IL_022a;
	}

IL_0167:
	{
		// xRect = texture.width - rect.width;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_32 = __this->___texture_2;
		NullCheck(L_32);
		int32_t L_33;
		L_33 = VirtualFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_32);
		float L_34;
		L_34 = Rect_get_width_m620D67551372073C9C32C4C4624C2A5713F7F9A9((&V_2), NULL);
		V_3 = ((float)il2cpp_codegen_subtract(((float)L_33), L_34));
		// break;
		goto IL_022a;
	}

IL_0181:
	{
		// yRect = texture.height - rect.height;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_35 = __this->___texture_2;
		NullCheck(L_35);
		int32_t L_36;
		L_36 = VirtualFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_35);
		float L_37;
		L_37 = Rect_get_height_mE1AA6C6C725CCD2D317BD2157396D3CF7D47C9D8((&V_2), NULL);
		V_4 = ((float)il2cpp_codegen_subtract(((float)L_36), L_37));
		// break;
		goto IL_022a;
	}

IL_019c:
	{
		// xRect = texture.width - rect.width;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_38 = __this->___texture_2;
		NullCheck(L_38);
		int32_t L_39;
		L_39 = VirtualFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_38);
		float L_40;
		L_40 = Rect_get_width_m620D67551372073C9C32C4C4624C2A5713F7F9A9((&V_2), NULL);
		V_3 = ((float)il2cpp_codegen_subtract(((float)L_39), L_40));
		// yRect = texture.height - rect.height;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_41 = __this->___texture_2;
		NullCheck(L_41);
		int32_t L_42;
		L_42 = VirtualFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_41);
		float L_43;
		L_43 = Rect_get_height_mE1AA6C6C725CCD2D317BD2157396D3CF7D47C9D8((&V_2), NULL);
		V_4 = ((float)il2cpp_codegen_subtract(((float)L_42), L_43));
		// break;
		goto IL_022a;
	}

IL_01c9:
	{
		// float tempWidth = texture.width - rect.width;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_44 = __this->___texture_2;
		NullCheck(L_44);
		int32_t L_45;
		L_45 = VirtualFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_44);
		float L_46;
		L_46 = Rect_get_width_m620D67551372073C9C32C4C4624C2A5713F7F9A9((&V_2), NULL);
		V_7 = ((float)il2cpp_codegen_subtract(((float)L_45), L_46));
		// float tempHeight = texture.height - rect.height;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_47 = __this->___texture_2;
		NullCheck(L_47);
		int32_t L_48;
		L_48 = VirtualFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_47);
		float L_49;
		L_49 = Rect_get_height_mE1AA6C6C725CCD2D317BD2157396D3CF7D47C9D8((&V_2), NULL);
		V_8 = ((float)il2cpp_codegen_subtract(((float)L_48), L_49));
		// xRect = tempWidth > texture.width ? 0 : tempWidth;
		float L_50 = V_7;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_51 = __this->___texture_2;
		NullCheck(L_51);
		int32_t L_52;
		L_52 = VirtualFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_51);
		if ((((float)L_50) > ((float)((float)L_52))))
		{
			goto IL_0209;
		}
	}
	{
		float L_53 = V_7;
		G_B19_0 = L_53;
		goto IL_020e;
	}

IL_0209:
	{
		G_B19_0 = (0.0f);
	}

IL_020e:
	{
		V_3 = G_B19_0;
		// yRect = tempHeight > texture.height ? 0 : tempHeight;
		float L_54 = V_8;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_55 = __this->___texture_2;
		NullCheck(L_55);
		int32_t L_56;
		L_56 = VirtualFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_55);
		if ((((float)L_54) > ((float)((float)L_56))))
		{
			goto IL_0223;
		}
	}
	{
		float L_57 = V_8;
		G_B22_0 = L_57;
		goto IL_0228;
	}

IL_0223:
	{
		G_B22_0 = (0.0f);
	}

IL_0228:
	{
		V_4 = G_B22_0;
	}

IL_022a:
	{
		// if (texture.width < rect.x + rect.width || texture.height < rect.y + rect.height ||
		//     xRect > rect.x + texture.width || yRect > rect.y + texture.height ||
		//     xRect < 0 || yRect < 0 || rect.width < 0 || rect.height < 0)
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_58 = __this->___texture_2;
		NullCheck(L_58);
		int32_t L_59;
		L_59 = VirtualFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_58);
		float L_60;
		L_60 = Rect_get_x_mB267B718E0D067F2BAE31BA477647FBF964916EB((&V_2), NULL);
		float L_61;
		L_61 = Rect_get_width_m620D67551372073C9C32C4C4624C2A5713F7F9A9((&V_2), NULL);
		if ((((float)((float)L_59)) < ((float)((float)il2cpp_codegen_add(L_60, L_61)))))
		{
			goto IL_02c0;
		}
	}
	{
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_62 = __this->___texture_2;
		NullCheck(L_62);
		int32_t L_63;
		L_63 = VirtualFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_62);
		float L_64;
		L_64 = Rect_get_y_mC733E8D49F3CE21B2A3D40A1B72D687F22C97F49((&V_2), NULL);
		float L_65;
		L_65 = Rect_get_height_mE1AA6C6C725CCD2D317BD2157396D3CF7D47C9D8((&V_2), NULL);
		if ((((float)((float)L_63)) < ((float)((float)il2cpp_codegen_add(L_64, L_65)))))
		{
			goto IL_02c0;
		}
	}
	{
		float L_66 = V_3;
		float L_67;
		L_67 = Rect_get_x_mB267B718E0D067F2BAE31BA477647FBF964916EB((&V_2), NULL);
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_68 = __this->___texture_2;
		NullCheck(L_68);
		int32_t L_69;
		L_69 = VirtualFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_68);
		if ((((float)L_66) > ((float)((float)il2cpp_codegen_add(L_67, ((float)L_69))))))
		{
			goto IL_02c0;
		}
	}
	{
		float L_70 = V_4;
		float L_71;
		L_71 = Rect_get_y_mC733E8D49F3CE21B2A3D40A1B72D687F22C97F49((&V_2), NULL);
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_72 = __this->___texture_2;
		NullCheck(L_72);
		int32_t L_73;
		L_73 = VirtualFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_72);
		if ((((float)L_70) > ((float)((float)il2cpp_codegen_add(L_71, ((float)L_73))))))
		{
			goto IL_02c0;
		}
	}
	{
		float L_74 = V_3;
		if ((((float)L_74) < ((float)(0.0f))))
		{
			goto IL_02c0;
		}
	}
	{
		float L_75 = V_4;
		if ((((float)L_75) < ((float)(0.0f))))
		{
			goto IL_02c0;
		}
	}
	{
		float L_76;
		L_76 = Rect_get_width_m620D67551372073C9C32C4C4624C2A5713F7F9A9((&V_2), NULL);
		if ((((float)L_76) < ((float)(0.0f))))
		{
			goto IL_02c0;
		}
	}
	{
		float L_77;
		L_77 = Rect_get_height_mE1AA6C6C725CCD2D317BD2157396D3CF7D47C9D8((&V_2), NULL);
		if ((!(((float)L_77) < ((float)(0.0f)))))
		{
			goto IL_02cb;
		}
	}

IL_02c0:
	{
		// throw new ArgumentException("Set value crop less than origin texture size");
		ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263* L_78 = (ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_tAD90411542A20A9C72D5CDA3A84181D8B947A263_il2cpp_TypeInfo_var)));
		NullCheck(L_78);
		ArgumentException__ctor_m026938A67AF9D36BB7ED27F80425D7194B514465(L_78, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral94E291225930DD4FDD61CA46BA3323392AF4D07C)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_78, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCropSquareU3Ed__2_MoveNext_mC58C96CCC338EA44F5D3BA39E81E547C28E15094_RuntimeMethod_var)));
	}

IL_02cb:
	{
		// result.SetPixels(texture.GetPixels(Mathf.FloorToInt(xRect), Mathf.FloorToInt(yRect),
		//                                 Mathf.FloorToInt(widthRect), Mathf.FloorToInt(heightRect)));
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_79 = __this->___U3CresultU3E5__2_5;
		WebCamTexture_t2021D179149C925AA6D73E6F1898C8D46521C749* L_80 = __this->___texture_2;
		float L_81 = V_3;
		int32_t L_82;
		L_82 = Mathf_FloorToInt_m2A39AE881CAEE6B6A4B3BFEF9CA1ED40625F5AB7_inline(L_81, NULL);
		float L_83 = V_4;
		int32_t L_84;
		L_84 = Mathf_FloorToInt_m2A39AE881CAEE6B6A4B3BFEF9CA1ED40625F5AB7_inline(L_83, NULL);
		float L_85 = V_5;
		int32_t L_86;
		L_86 = Mathf_FloorToInt_m2A39AE881CAEE6B6A4B3BFEF9CA1ED40625F5AB7_inline(L_85, NULL);
		float L_87 = V_6;
		int32_t L_88;
		L_88 = Mathf_FloorToInt_m2A39AE881CAEE6B6A4B3BFEF9CA1ED40625F5AB7_inline(L_87, NULL);
		NullCheck(L_80);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_89;
		L_89 = WebCamTexture_GetPixels_mD30134473515AEA70C9DE43392F2ADD95747237A(L_80, L_82, L_84, L_86, L_88, NULL);
		NullCheck(L_79);
		Texture2D_SetPixels_mAE0CDFA15FA96F840D7FFADC31405D8AF20D9073(L_79, L_89, NULL);
		// yield return null;
		__this->___U3CU3E2__current_1 = NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)NULL);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_030c:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// result.Apply();
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_90 = __this->___U3CresultU3E5__2_5;
		NullCheck(L_90);
		Texture2D_Apply_mA014182C9EE0BBF6EEE3B286854F29E50EB972DC(L_90, NULL);
	}

IL_031e:
	{
		// yield return null;
		__this->___U3CU3E2__current_1 = NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)NULL);
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_032e:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// callback(result);
		Action_1_tD7F74291FAA5A362F4A5F48D4E1CF3ED51FA6A33* L_91 = __this->___callback_4;
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_92 = __this->___U3CresultU3E5__2_5;
		NullCheck(L_91);
		Action_1_Invoke_mCBAA13E9863B2958F53D13FBB8B1B51CA0011B53_inline(L_91, L_92, NULL);
		// }
		return (bool)0;
	}
}
// System.Object TFClassify.TextureTools/<CropSquare>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CCropSquareU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18CC5480C17CDC221D5A7941F4E9A99858D1510B (U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void TFClassify.TextureTools/<CropSquare>d__2::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCropSquareU3Ed__2_System_Collections_IEnumerator_Reset_m08EFD2ED4457D7E2CCF13F35B9204722A3493FDB (U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCropSquareU3Ed__2_System_Collections_IEnumerator_Reset_m08EFD2ED4457D7E2CCF13F35B9204722A3493FDB_RuntimeMethod_var)));
	}
}
// System.Object TFClassify.TextureTools/<CropSquare>d__2::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CCropSquareU3Ed__2_System_Collections_IEnumerator_get_Current_mF8DD69C9A46B4CDDDE50791F9CE1372EACB1BB23 (U3CCropSquareU3Ed__2_t37D32979A9ED5461860D4E6DE05E92F8A7FD4D2F* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* BoundingBox_get_Dimensions_m4F3FC84E34A2F040280CA9119B6AE2C9222F55AC_inline (BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096* __this, const RuntimeMethod* method) 
{
	{
		// public BoundingBoxDimensions Dimensions { get; set; }
		BoundingBoxDimensions_t78E5B4C66816C7753FABD96763BA0A9403A35086* L_0 = __this->___U3CDimensionsU3Ek__BackingField_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float DimensionsBase_get_X_m2AA39E00503C51C587F473D1ADBEC9CCB5D54F41_inline (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, const RuntimeMethod* method) 
{
	{
		// public float X { get; set; }
		float L_0 = __this->___U3CXU3Ek__BackingField_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float DimensionsBase_get_Y_m170190F8D1531F544A40AB55817C1EC7EFD8A864_inline (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, const RuntimeMethod* method) 
{
	{
		// public float Y { get; set; }
		float L_0 = __this->___U3CYU3Ek__BackingField_1;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float DimensionsBase_get_Width_m6B20834F813A905E59F902C802FAB97D21286ECF_inline (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, const RuntimeMethod* method) 
{
	{
		// public float Width { get; set; }
		float L_0 = __this->___U3CWidthU3Ek__BackingField_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float DimensionsBase_get_Height_m6632C66BBCA8F5D5ABC7E95739DD3713DBA7E9BE_inline (DimensionsBase_tEBF7DCBFCC12247500302674DC28326F959D8303* __this, const RuntimeMethod* method) 
{
	{
		// public float Height { get; set; }
		float L_0 = __this->___U3CHeightU3Ek__BackingField_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* BoundingBox_get_Label_m6C86B30C4199D01AA9838287FEA1580EA9C3DA2D_inline (BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096* __this, const RuntimeMethod* method) 
{
	{
		// public string Label { get; set; }
		String_t* L_0 = __this->___U3CLabelU3Ek__BackingField_1;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float BoundingBox_get_Confidence_m7930AB5EF72D84E6FCFB1432F5F62F169BB1C3A2_inline (BoundingBox_t4AF3E306CA2CD48494B5954947E6778D7F153096* __this, const RuntimeMethod* method) 
{
	{
		// public float Confidence { get; set; }
		float L_0 = __this->___U3CConfidenceU3Ek__BackingField_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_2 = L_0;
		float L_1 = ___y1;
		__this->___y_3 = L_1;
		float L_2 = ___z2;
		__this->___z_4 = L_2;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) 
{
	{
		float L_0 = ___r0;
		__this->___r_0 = L_0;
		float L_1 = ___g1;
		__this->___g_1 = L_1;
		float L_2 = ___b2;
		__this->___b_2 = L_2;
		float L_3 = ___a3;
		__this->___a_3 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color32__ctor_mC9C6B443F0C7CA3F8B174158B2AF6F05E18EAC4E_inline (Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B* __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method) 
{
	{
		__this->___rgba_0 = 0;
		uint8_t L_0 = ___r0;
		__this->___r_1 = L_0;
		uint8_t L_1 = ___g1;
		__this->___g_2 = L_1;
		uint8_t L_2 = ___b2;
		__this->___b_3 = L_2;
		uint8_t L_3 = ___a3;
		__this->___a_4 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_FloorToInt_m2A39AE881CAEE6B6A4B3BFEF9CA1ED40625F5AB7_inline (float ___f0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		float L_0 = ___f0;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = floor(((double)L_0));
		V_0 = il2cpp_codegen_cast_double_to_int<int32_t>(L_1);
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_mF2422B2DD29F74CE66F791C3F68E288EC7C3DB9E_gshared_inline (Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* __this, RuntimeObject* ___obj0, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___obj0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
